import cv2
img=cv2.imread('images.jfif')


#img = cv2.line(img,(0,0),(255,255),(0,0,255),5)
#img = cv2.arrowedLine(img,(0,0),(255,255),(255,0,0),5)
img =cv2.rectangle(img,(150,320),(400,0),(0,0,255),3)
#img2 = cv2.circle(img,(447,63),63,(0,255,0),50)
img = cv2.putText(img,"OpenCV",(10,100),cv2.FONT_HERSHEY_SIMPLEX,1,(0,0,255),2)
img = cv2.putText(img,"(James Gosling)",(150,350),cv2.FONT_HERSHEY_SIMPLEX,1,(255,0,0),2)
cv2.imshow("Result", img)

cv2.waitKey(0)
cv2.destroyAllWindows()
