import cv2

faceCascade=cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
eyeCascade=cv2.CascadeClassifier("haarcascade_eye.xml")
#noseCascade=cv2.CascadeClassifier("Nariz.xml")
#mouthCascade=cv2.CascadeClassifier("Mouth.xml")
smileCascade=cv2.CascadeClassifier("haarcascade_smile.xml")

def draw_boundary(img,classifier,scaleFactor,minNeighbors,color,text):
    gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    features=classifier.detectMultiScale(gray,scaleFactor,minNeighbors)
    coords=[]
    for (x,y,w,h) in features:
        cv2.rectangle(img,(x,y),(x+w,y+h),color,2)
        cv2.putText(img,text,(x,y-4),cv2.FONT_HERSHEY_SIMPLEX,1,color,1)
        coords=[x,y,w,h]
    return img

def detect(img,faceCascade,eyeCascade,smileCascade):
        img=draw_boundary(img,faceCascade,1.1,10,(0,0,255),"Face")
        img=draw_boundary(img,eyeCascade,1.1,15,(255,0,0),"Eye")
        #img=draw_boundary(img,smileCascade,1.1,50,(255,0,0),"Smile") 
       # img=mouthCascade(img,mouthCascade,1.1,20,(255,255,0),"Mouth")
        return img
    

cap = cv2.VideoCapture("Video.mp4")
while (True):
    ret,frame = cap.read()
    frame=detect(frame,faceCascade,eyeCascade,smileCascade)
    cv2.imshow('frame',frame)
    if(cv2.waitKey(1) & 0xFF== ord('q')):
        break
cap.release()
cv2.destroyAllWindows()


