import logging
import os
from datetime import datetime 

cwd = os.getcwd()
PATH_LOGS = cwd+'\logs'

if not os.path.exists(PATH_LOGS):
    os.makedirs(PATH_LOGS)


now = datetime.now()
dt_string = now.strftime("%Y-%m-%d")
format_str = '%(asctime)s\t%(levelname)8s -- %(processName)s %(filename)s:%(lineno)4s -- %(message)s'

logging.basicConfig(level=logging.DEBUG,
                    format=format_str,
                    datefmt='%Y-%m-%d %H:%M:%S',
                    filename=PATH_LOGS+'/'+dt_string+'.log')

# define a Handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler()
# console.setLevel(logging.INFO)
# set a format which is simpler for console use
formatter = logging.Formatter('%(asctime)s\t%(levelname)8s %(processName)-12s:%(lineno)4s: %(message)s')
# tell the handler to use this format
console.setFormatter(formatter)
# add the handler to the root logger
logging.getLogger('').addHandler(console)

# Now, we can log to the root logger, or any other logger. First the root...
# logging.info('Strat Logs')

# Now, define a couple of other loggers which might represent areas in your
log = logging.getLogger()
# log = logging.getLogger()  # 'root' Logger
# log.addHandler(console)  # prints to console.
# log.setLevel(logging.ERROR) # anything ERROR or above
# log.setLevel(logging.DEBUG)  # anything DEBUG
log.setLevel(logging.INFO) # anything INFO 
# log.info('Show example log level info')
# log.warn('Show example log level warning')
# log.critical('Show example log level critical')
# log.error('Show example log level error')
# log.debug('Show example log level debug')
# file = 'ss'
