import pygame
import win32api
import win32con
import win32gui

pygame.init()
screen = pygame.display.set_mode((800, 600)) # For borderless, use pygame.NOFRAME
done = False
fuchsia = (255, 0, 128)  # Transparency color
dark_red = (139, 0, 0)

# Set window transparency color
hwnd = pygame.display.get_wm_info()["window"]
win32gui.SetWindowLong(hwnd, win32con.GWL_EXSTYLE,
                       win32gui.GetWindowLong(hwnd, win32con.GWL_EXSTYLE) | win32con.WS_EX_LAYERED)
win32gui.SetLayeredWindowAttributes(hwnd, win32api.RGB(*fuchsia), 0, win32con.LWA_COLORKEY)

i = 0
x=50
y=50


while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    screen.fill(fuchsia)  # Transparent background
    while i<5: 
        pygame.draw.rect(screen, dark_red, pygame.Rect(x, y, 60, 60),1)
        pygame.display.update()
        x+=50
        y+=50
        i+=1

