@ECHO OFF
REM setlocal EnableDelayedExpansion 

IF [%1]==[] GOTO :help
IF [%1]==[/?] GOTO :help
IF [%1]==[--help] GOTO :help
IF [%1]==[-t] GOTO :testmode

echo %* |find "/?" > nul
IF errorlevel 1 GOTO :main

:help
ECHO & echo.
ECHO Usage:  .\bat_run_screenshot.bat [PATH_EXCEL] [OPTIONS] & echo.       
ECHO Options: [PATH_EXCEL]: Path excel template 
ECHO          -e [Edit template excel]
ECHO          -s [start excel after finishing the process]
ECHO          -t [test script mode] & echo.

GOTO :end
 
:main
..\Python38-32\python.exe run_py.py run_py.py -p %1 %2 %3 %4

exit /b %errorlevel% 

if %errorlevel% EQU 0 (
   echo Success
) else (
   echo Failure Reason Given is %errorlevel%
   exit /b %errorlevel%
)

:end
exit /b %errorlevel% 

:testmode
ECHO "TEST MODE STARTING..."
..\Python38-32\python.exe run_py.py -p template\Template_image.xlsx 

GOTO :end

REM PAUSE

REM set argCount=0 
REM for %%x in (%*) do (
REM    set /A argCount+=1
REM    set "argVec[!argCount!]=%%~x"
REM )

REM echo Number of processed arguments: %argCount%

REM set Is_arg=True
REM if %argCount% LSS 1 SET  Is_arg=False
REM if %argCount% GTR 3 SET  Is_arg=False
 

REM if "%Is_arg%" == "False" (
REM    echo Failure Reason Given is %errorlevel%
REM    exit /b %errorlevel%
REM ) 

REM for /L %%i in (1,1,%argCount%) do (
REM     if %%i EQU 1 SET arg1="!argVec[%%i]!"
REM     if "!argVec[%%i]!" == "-e" SET arg2=!argVec[%%i]!
     
REM )
REM echo %arg1%
REM echo %arg2%