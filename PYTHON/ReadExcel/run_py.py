import glob
from os.path import expanduser
import tkinter as tk
from tkinter import filedialog
import pandas as pd
import xlrd
import xlwt
import sys
import random
import os.path
from os import path
from log_file import log
from pynput import keyboard
from pynput import mouse
from pynput.keyboard import Key, Listener, Controller
import xlsxwriter
import wx
import win32gui
import win32api
import win32con
from win32api import GetSystemMetrics
import time
import openpyxl
import subprocess
import pyautogui
import datetime
import PIL
import shutil
import pygame


keyboards = Controller()

# print(sys.argv)
# check argument
if len(sys.argv) < 3 or len(sys.argv) > 5:
    log.warning(
        'Invalid parameters: run_py.py -p <PATH_EXCLE> [-e <EDIT POSITION>]')
    exit(0)

# PATH_EXCEL_TEMPLATE = expanduser("~")
# print('PATH_EXCEL:', PATH_EXCEL_TEMPLATE)

PATH_EXCEL_TEMPLATE = ''
EDIT_MODE = False
IS_START_EXCEL = False
cwd = os.getcwd()

a = 0
for i in sys.argv:
    if i == '-p':
        PATH_EXCEL_TEMPLATE = sys.argv[a+1]
    if i == '-e':
        EDIT_MODE = True
    if i == '-s':
        IS_START_EXCEL = True
    a += 1
log.info('PATH_EXCEL_TEMPLATE: %s' % PATH_EXCEL_TEMPLATE)
log.info('EDIT_MODE: %s' % EDIT_MODE)

template_name_1 = PATH_EXCEL_TEMPLATE.split('\\')
template_name_2 = template_name_1[len(template_name_1)-1]
template_name_3 = str(template_name_2).split('.')
template_name = str(template_name_3[0])


PATH_IMAGE_ORDER = cwd+"\order_img"
PATH_IMAGE_DESTINATION = cwd+"\screenshot"
PATH_EXCELS = cwd+"\output\excel"
PATH_LOGS = cwd+'\logs'

excel_name = 'DailyOpereitonReport'
sheet_name = 'REPORT'


DIC_POSITION = {}
DIC_CELL_PIC = {}
DIC_CELL_XY = {}
KEY_WORD_PIC = '{picture'
KEY_WORD_POSI = '{XY:'
NUMBER_OF_PIC = 0
NUMBER_OF_XY = 0
NUMBER_LOOP_CHK = 0
CHK_SCREENSHOT = True
listener_running = False
OUTPUT_FILE = ''

NUMBER_PIC = 0
NUMBER_POSI = 0
POSI_VALUE = ''
CHK_POSI = False
F9_KEY = False
F10_KEY = False
ON_CLICK = False
IS_REMOVE_XY = False
MODE = 0
done = False
px = 0
py = 0
loop_perpig = 1
np = 3
i = 0
posi = ''
position = ''
POSITION_FINAL = ''
IS_VALIDATE = False
ROW_XY = ''
COL_XY = ''
timer = 0

now_two = ''


class Desktop(wx.Frame):
    def __init__(self, x, y, w, h):
        super(Desktop, self).__init__(None, -1, '')
        print('set_xy:', x, y, w, h)

        self.AssociateHandle(win32gui.GetDesktopWindow())
        dc = wx.WindowDC(self)
        dc.SetPen(wx.Pen("grey", style=wx.TRANSPARENT))
        dc.SetBrush(wx.Brush("grey", wx.SOLID))
        dc.DrawRectangle(int(x), int(y), int(w), int(h))
        # self.Destroy()


def get_dateNow():
    now = datetime.datetime.now()
    now_two_param = str(now).split(" ")
    date = str(now_two_param[0])
    raw_time = str(now_two_param[1]).replace('.', '')
    time_clean = raw_time.replace(':', '')
    return date+'_'+time_clean


def ReadExcel():
    global NUMBER_OF_PIC
    global NUMBER_OF_XY
    global NUMBER_PIC
    global NUMBER_POSI
    global POSI_VALUE
    global CHK_POSI
    global ROW_XY
    global COL_XY

    chk_excel = path.exists(PATH_EXCEL_TEMPLATE)
    if not chk_excel:
        log.error("No such file : \'%s\'" % PATH_EXCEL_TEMPLATE)
        exit(0)

    # To open Workbook
    wb = xlrd.open_workbook(PATH_EXCEL_TEMPLATE)
    sheet = wb.sheet_by_index(0)
    # sheet.write(4, 4, 'HHH')
    for row_id in range(sheet.nrows):
        for col_id in range(sheet.ncols):
            value = sheet.cell_value(row_id, col_id)
            print(value)
            if KEY_WORD_PIC in str(value):
                # log.info('%s row= %d , col= %d' % (value, row_id, col_id))
                NUMBER_PIC += 1
                DIC_POSITION.update({value: '{blank}'})
                DIC_CELL_PIC.update({value: str(row_id)+','+str(col_id)})
                try:
                    rowXY_id = row_id+1
                    valueXY = sheet.cell_value(rowXY_id, col_id)
                    if KEY_WORD_POSI in valueXY:
                        DIC_POSITION.update({value: valueXY})
                        DIC_CELL_XY.update(
                            {value: str(rowXY_id)+','+str(col_id)})  # valueXY
                        # log.info('%s row= %d , col= %d' %
                        #          (valueXY, row_id, col_id))
                except Exception as e:
                    print(e)

    NUMBER_OF_PIC = len(DIC_CELL_PIC)
    NUMBER_OF_XY = len(DIC_CELL_XY)

    print("Modified Dict : ")
    for (key, value) in DIC_POSITION.items():
        print(key, " :: ", value)
    print('^len DIC_POSITION:', len(DIC_POSITION))

    for (key, value) in DIC_CELL_PIC.items():
        print(key, " :: ", value)
    print('^len DIC_CELL_PIC:', len(DIC_CELL_PIC))

    for (key, value) in DIC_CELL_XY.items():
        print(key, " :: ", value)
    print('^len DIC_CELL_XY:', len(DIC_CELL_XY))


def on_move(x, y):
    global loop_perpig
    if loop_perpig == 2:
        positionStr = 'W: ' + str(x-px).rjust(4) + ' H: ' + str(y-py).rjust(4)
        print(positionStr, end='')
        print('\b' * len(positionStr), end='', flush=True)
    else:
        positionStr = 'X: ' + str(x).rjust(4) + ' Y: ' + str(y).rjust(4)
        print(positionStr, end='')
        print('\b' * len(positionStr), end='', flush=True)


def add_value(valueXY):
    global DIC_POSITION
    print('add_value')
    m = MODE
    if m == 1 or m == 3:
        print('Mode = 1 || 3')
        for (key, value) in DIC_POSITION.items():
            if 'blank' in value:
                DIC_POSITION.update({key: valueXY})
                break
    elif m == 2:
        print('m == 2')
        # print (DIC_CELL_XY)
        for (key, value) in DIC_POSITION.items():
            if 'blank' in value:
                DIC_POSITION.update({key: valueXY})
                break
    # elif m == 3:
    #     print('m == 3')


def on_click(x, y, button, pressed):
    global done
    global NUMBER_LOOP_CHK
    global F10_KEY
    global IS_REMOVE_XY
    global POSI_VALUE
    if ON_CLICK:

        global posi
        global position
        positionStr = ''
        global i
        global loop_perpig
        global px
        global py

        if button == mouse.Button.left:
            if pressed:
                done = True
                if loop_perpig == 2:
                    F10_KEY = True
                    px = x-px
                    py = y-py

                    x = str(px).rjust(4)
                    y = str(py).rjust(4)
                    if int(x) < 0 or int(y) < 0:
                        log.warning(
                            'invalid position Width Height in picture!')
                        log.warning(
                            'Pless select Width Height in display again then press \'f10\'')
                    else:
                        positionStr = 'W: ' + x + ' H: ' + y
                        position = position+x+','+y
                        loop_perpig = 0
                        i += 1
                        loop_perpig += 1
                        log.info(positionStr)
                        refac_spec_1 = ' '.join(position.split())
                        refac_spec_2 = refac_spec_1.replace(' ', '')
                        position = refac_spec_2
                        print('position onclick:', position)

                        add_value(position)
                        position = ''  # initiale
                        NUMBER_LOOP_CHK += 1

                        for (key, value) in DIC_POSITION.items():
                            if 'blank' in value:
                                print('Select your::', key)
                                break

                        if NUMBER_LOOP_CHK == NUMBER_OF_PIC:
                            on_key(keyboard.Key.f10)
                else:

                    px = x
                    py = y
                    x = str(px).rjust(4)
                    y = str(py).rjust(4)

                    if int(x) < 0 or int(y) < 0:
                        log.warning('invalid position X Y in picture!')
                        log.warning(
                            'Pless select X , Y in display again')
                    else:
                        positionStr = 'X: ' + x + ' Y: ' + y
                        position = position+x+','+y+','

                        i += 1
                        loop_perpig += 1
                        log.info(positionStr)


def write_posi_excel():
    global COL_XY
    global ROW_XY
    global POSITION_FINAL
    global DIC_POSITION
    print('write_posi_excel')

    # open the file for reading
    from os.path import expanduser
    # open the file for reading
    wbRD = xlrd.open_workbook(PATH_EXCEL_TEMPLATE)
    sheets = wbRD.sheets()

    # open the same file for writing (just don't write yet)
    wb = xlsxwriter.Workbook(PATH_EXCEL_TEMPLATE)

    # run through the sheets and store sheets in workbook
    # this still doesn't write to the file yet
    for sheet in sheets:  # write data from old file
        newSheet = wb.add_worksheet(sheet.name)
        for row in range(sheet.nrows):
            for col in range(sheet.ncols):
                newSheet.write(row, col, sheet.cell(row, col).value)
    wb.close()  # THIS writes

    # loading XL sheet bassed on file name provided by user
    book = openpyxl.load_workbook(PATH_EXCEL_TEMPLATE)
    # opening sheet whose index no is 0
    sheet = book.worksheets[0]

    # insert_rows(idx, amount=1) Insert row or rows before row==idx, amount will be no of
    # rows you want to add and it's optional
    if MODE == 1:
        print('Write mode 2')
        for key, value in reversed(DIC_CELL_PIC.items()):
            cell = str(value).split(',')
            row = int(cell[0])+1
            col = int(cell[1])+1
            row_insert = int(row)+1
            print('insert row=', row_insert)

            refac_1 = DIC_POSITION.get(key).replace(' ', ',')
            refac_2 = '{XY:'+refac_1+'}'
            value_xy = refac_2

            sheet.insert_rows(int(row_insert))
            sheet.cell(row=int(row_insert), column=int(
                col)).value = value_xy
    if MODE == 2 or MODE == 3:
        print('Write mode 2')
        for (key, value) in reversed(DIC_POSITION.items()):
            refac_1 = str(value).replace(' ', ',')
            if IS_REMOVE_XY:
                refac_2 = '{XY:'+refac_1+'}'
            else:
                refac_2 = refac_1
            value_xy = refac_2
            cell_value = DIC_CELL_XY.get(key)
            # print("cell_value = ", cell_value)
            if cell_value is None:
                refac_2 = '{XY:'+refac_1+'}'
                value_xy = refac_2
                # print('cell_value = NONNNNN')
                cell_value = DIC_CELL_PIC.get(key)
                cell = str(cell_value).split(',')
                row = int(cell[0])+1
                col = int(cell[1])+1
                row_insert = int(row)+1
                sheet.insert_rows(int(row_insert))
                sheet.cell(row=int(row_insert),
                           column=int(col)).value = value_xy
            else:
                # value_xy = refac_2
                cell = str(cell_value).split(',')
                row = int(cell[0])+1
                col = int(cell[1])+1
                sheet.cell(row=int(row), column=int(col)).value = value_xy

    book.save(PATH_EXCEL_TEMPLATE)


def validate():
    global IS_VALIDATE
    global F9_KEY
    global position
    global NUMBER_POSI
    global ON_CLICK
    global DIC_POSITION
    print('validate picture...')
    IS_VALIDATE = True

    for (key, value) in DIC_POSITION.items():
        print(key, " :: ", value)
        if 'blank' in value:
            IS_VALIDATE = False
            log.warning(
                'Number of position[XY] not match to Number of picture!')
        else:
            F9_KEY = False
            ON_CLICK = False
    print('^len DIC_POSITION:', len(DIC_POSITION))
    if IS_VALIDATE:
        print('Validate is passed.')
        write_posi_excel()


def on_key(key):
    global POSITION_FINAL
    global F9_KEY
    global F10_KEY
    global ON_CLICK
    global CHK_SCREENSHOT
    if F9_KEY:
        if (key == keyboard.Key.f9):
            ON_CLICK = True
            log.info('Press Esc to quit.')
            log.info('Press f10 to continue.')
            for (key, value) in DIC_POSITION.items():
                if 'blank' in value:
                    print('Select your::', key)
                    break
            F9_KEY = False

    if (key == keyboard.Key.esc):
        print('esc')
        listener.stop()
        key_listener.stop()

    if F10_KEY:
        if (key == keyboard.Key.f10):
            print('Press:: ', key)
            F10_KEY = False
            # print('Press f10')
            if loop_perpig != 1:
                F9_KEY = False
                log.warning('invalid position in picture!')
                log.warning(
                    'Pless select last position in display then press \'f10\'')
            else:
                F9_KEY = False

                validate()
                log.info('Success...')
                flow_screenshot()


def start_excel(path_excel):
    print('start_excel')
    os.system('start excel.exe "%s"' % path_excel)
    # from win32com.client import Dispatch

    # xl = Dispatch("Excel.Application")
    # xl.Visible = True  # otherwise excel is hidden

    # # newest excel does not accept forward slash in path
    # wb = xl.Workbooks.Open(path_excel)
    # wb.Close()
    # xl.Quit()


def flow_screenshot():
    global listener_running
    ReadExcel()
    screenShot()
    move_to_excel()
    log.info('successfully.\n')
    print('IS_START_EXCEL=', IS_START_EXCEL)

    if IS_START_EXCEL:
        start_excel(OUTPUT_FILE)

    if not listener_running:
        exit(0)

    listener.stop()
    key_listener.stop()


def OpenApp(i):
    global F9_KEY
    if i == 1:
        F9_KEY = True
        print('Please switch to the application in which you want to take a screenshot. then press F9 to select position.')
    elif i == 2:
        print('Pless select position in display again then press \'f10\'')
    elif i == 3:
        F9_KEY = True
        print('Please switch to the application in which you want to take a screenshot. then press F9 to select position.')
        print('Pless select position in display [%d] more times then press \'f10\'' % (
            NUMBER_OF_PIC - NUMBER_OF_XY))


def on_clicks(x, y, button, pressed):
    global done
    if button == mouse.Button.left:
        if pressed:
            print(button)
            done = True
            return False


def random_color():
    r = random.randint(0, 255)
    g = random.randint(0, 255)
    b = random.randint(0, 255)
    rgb = [r, g, b]
    return rgb


def draw_box():
    global done
    W = GetSystemMetrics(0)
    H = GetSystemMetrics(1)
    print("Width =", W)
    print("Height =", H)
    pygame.init()
    # For borderless, use pygame.NOFRAME
    screen = pygame.display.set_mode((W, H), pygame.FULLSCREEN)
    done = False
    fuchsia = (255, 0, 128)  # Transparency color

    # Set window transparency color
    hwnd = pygame.display.get_wm_info()["window"]
    win32gui.SetWindowLong(hwnd, win32con.GWL_EXSTYLE,
                           win32gui.GetWindowLong(hwnd, win32con.GWL_EXSTYLE) | win32con.WS_EX_LAYERED)
    win32gui.SetLayeredWindowAttributes(
        hwnd, win32api.RGB(*fuchsia), 0, win32con.LWA_COLORKEY)
    screen.fill(fuchsia)  # Transparent background

    CHK = True
    py = 0
    while not done:
        if CHK:
            for key, value in DIC_POSITION.items():
                CHK = False
                line_color = random_color()
                myfont = pygame.font.SysFont('Comic Sans MS', 25)

                textsurface = myfont.render(
                    key, False, (line_color), (255, 255, 255))
                var_1 = value.split(':')
                var_2 = var_1[1].replace('}', '')
                position_display = var_2.split(',')
                x = position_display[0]
                y = position_display[1]
                w = position_display[2]
                h = position_display[3]
                screen.blit(textsurface, (0, py))  # draw text
                py += 50
                pygame.draw.rect(screen, line_color, pygame.Rect(
                    int(x), int(y), int(w), int(h)), 5)  # draw rect
                pygame.display.update()

        with mouse.Listener(on_click=on_clicks) as listener:
            try:
                listener.join()
            except Exception as e:
                print('Done'.format(e.args[0]))

    pygame.quit()


def yes_or_no(question):
    global IS_REMOVE_XY
    reply = str(input(question+' (y/n): ')).lower().strip()
    if not reply:
        return yes_or_no("Uhhhh... please enter ")
    if reply[0] == 'y':
        IS_REMOVE_XY = True
        print('Yes')
        return True
    if reply[0] == 'n':
        IS_REMOVE_XY = False
        print('No')
        return False
    else:
        IS_REMOVE_XY = False
        return yes_or_no("Uhhhh... please enter ")


def remove_dataxy():
    global DIC_POSITION
    global DIC_CELL_XY
    global NUMBER_LOOP_CHK
    global POSI_VALUE
    global NUMBER_POSI
    global NUMBER_PIC
    global position

    draw_box()
    chk_remove_xy = yes_or_no('Do you want to clear the old data?')

    if chk_remove_xy:  # Say Remove
        POSI_VALUE = ''
        NUMBER_POSI = 0
        for (key, value) in DIC_POSITION.items():
            DIC_POSITION.update({key: 'blank'})

        log.info('clear data success!')

        for (key, value) in DIC_POSITION.items():
            print('DIC_POSITION :: ', key, '::', value)
        for (key, value) in DIC_CELL_XY.items():
            print('DIC_CELL_XY  :: ', key, '::', value)
        OpenApp(1)
    else:  # Say Not Remove
        if NUMBER_OF_PIC == NUMBER_OF_XY:
            log.warning('You have reached your data amount.')
            log.info('Success...')
            flow_screenshot()
        else:
            print('this say no for remove')
            NUMBER_LOOP_CHK = NUMBER_OF_XY
            # ShowBoxPosition
            for (key, value) in DIC_POSITION.items():
                print(key, " :: ", value)
            print('^len DIC_POSITION:', len(DIC_POSITION))

            for (key, value) in DIC_CELL_PIC.items():
                print(key, " :: ", value)
            print('^len DIC_CELL_PIC:', len(DIC_CELL_PIC))

            for (key, value) in DIC_CELL_XY.items():
                print(key, " :: ", value)
            print('^len DIC_CELL_XY:', len(DIC_CELL_XY))
            OpenApp(3)


def screenShot():
    # global CHK_SCREENSHOT
    log.info('ScreenShot')
    screenshotName = "screenshot_"

    path = PATH_IMAGE_ORDER

    if not os.path.exists(path):
        os.makedirs(path)

    i = 0
    for key, value in DIC_POSITION.items():

        pic_1 = str(key).replace('{', '')
        pic_2 = pic_1.replace('}', '')
        pic_name = screenshotName+pic_2
        # print(value)
        # continue
        var_1 = value.split(':')
        # print(var_1[1])
        var_2 = var_1[1].replace('}', '')
        # print(var_2)
        position_display = var_2.split(',')
        # print(position_display, '::len::', len(position_display))

        if len(position_display) != 4:
            log.error('invalid :%s :: %s' % (key, value))
            break

        x_start_point = position_display[0]
        y_start_point = position_display[1]
        screenWidth = position_display[2]
        screenHeight = position_display[3]

        chk_screenxy = pyautogui.onScreen(
            int(x_start_point), int(y_start_point))
        chk_screenwh = pyautogui.onScreen(int(screenWidth), int(screenHeight))

        if not chk_screenxy or not chk_screenwh:
            log.error('invalid :%s :: %s' % (key, value))
            break

        log.info('x point:: %s px.' % x_start_point)
        log.info('y point:: %s px.' % y_start_point)
        log.info('screen_Width:: %s px.' % screenWidth)
        log.info('screen_Height:: %s px.' % screenHeight)
        log.info('..................')

        datetime_now = get_dateNow()

        try:
            log.info('taking a screenshot...')
            screenshot = pyautogui.screenshot(path + '/' + pic_name + '_' + datetime_now +
                                              ".png", region=(x_start_point, y_start_point, screenWidth, screenHeight))
        except Exception as e:
            # CHK_SCREENSHOT = False
            log.error(e)
            continue

        log.info("screenshot taken , parameters of  the file are:%s" %
                 str(screenshot))

        files_path = os.path.join(path, '*')
        files = sorted(glob.iglob(files_path),
                       key=os.path.getatime, reverse=True)

        last_screenshot = files[0]
        log.info("Lasted screenshot is: %s" % str(last_screenshot))

        log.info("Going to wait: %s seconds until next cycle" % str(timer))
        i += 1
        time.sleep((int(timer)))


def move_to_excel():
    global OUTPUT_FILE
    log.info('move_to_excel')

    if not os.path.exists(PATH_IMAGE_DESTINATION):
        os.makedirs(PATH_IMAGE_DESTINATION)
    if not os.path.exists(PATH_EXCELS):
        os.makedirs(PATH_EXCELS)

    date_time = get_dateNow()

    files_path = os.path.join(PATH_IMAGE_ORDER, '*')
    files = sorted(glob.iglob(files_path), key=os.path.getatime, reverse=True)
    log.info('image in order_img is: %d files.' % len(files))

    # Create an new Excel file and add a worksheet.
    excel_filename = template_name+'_'+date_time+'.xlsx'
    print('excel_filename=', excel_filename)
    # exit(0)
    OUTPUT_FILE = PATH_EXCELS+'\\'+excel_filename
    log.info('Creating... %s' % excel_filename)

    log.info('Write image to excel file please wait.')

    # open the file for reading
    wbRD = xlrd.open_workbook(PATH_EXCEL_TEMPLATE)
    sheets = wbRD.sheets()

    # open the same file for writing (just don't write yet)
    wb = xlsxwriter.Workbook(OUTPUT_FILE)

    font_format = wb.add_format({
        'font_color': '#D3D3D3'})

    # run through the sheets and store sheets in workbook
    # this still doesn't write to the file yet
    for sheet in sheets:  # write data from old file
        worksheet = wb.add_worksheet(sheet.name)
        for row in range(sheet.nrows):
            for col in range(sheet.ncols):
                values = sheet.cell(row, col).value
                if KEY_WORD_POSI in str(values):
                    worksheet.write(row, col, values, font_format)
                    continue
                worksheet.write(row, col, values)

    log.info('write new data')
    # i = 0
    for key, value in reversed(DIC_CELL_XY.items()):
        str_1 = str(value).split(',')
        row = int(str_1[0])
        col = int(str_1[1])

        pic_1 = str(key).replace('{', '')
        pic_name = pic_1.replace('}', '')

        for img in files:
            if pic_name in img:
                with PIL.Image.open(img) as im:
                    x, y = im.size

                width = x
                height = y
                # print(key)
                # print('w,h', width, height)
                # log.info('.')
                worksheet.insert_image(row, col, img, {
                    'x_scale': 0.5, 'y_scale': 0.5})
                worksheet.set_row(row, (height*0.375))
                break
                # i += 1

        # newSheet.write(row, col, "test ({}, {})".format(row, col))
    # for row in range(10, 20): # write NEW data
    #     for col in range(20):
    #         newSheet.write(row, col, "test ({}, {})".format(row, col))
    wb.close()  # THIS writes
    log.info('writing successful:: %s' % OUTPUT_FILE)
    log.info('Move images from %s to %s ' %
             (str(PATH_IMAGE_ORDER), str(PATH_IMAGE_DESTINATION)))

    for image in files:
        img_name = str(image).split('\\')
        img_name_final = img_name[len(img_name)-1]
        # print('O=',image)
        # print('D=',path_image_destination+"\\"+img_name_final)
        shutil.move(image, PATH_IMAGE_DESTINATION+"\\"+img_name_final)


# ReadExcel()
if EDIT_MODE:
    ReadExcel()
    log.info('Number of picture: %d' % len(DIC_CELL_PIC))
    log.info('Number of Position[X,Y]: %d' % len(DIC_CELL_XY))

    if NUMBER_OF_XY == 0:
        MODE = 1
        NUMBER_LOOP_CHK = 0
        OpenApp(1)
    elif NUMBER_OF_XY == NUMBER_OF_PIC:
        MODE = 3
        NUMBER_LOOP_CHK = 0
        log.warning('You have reached your data amount.')
        remove_dataxy()
        # exit(0)
#         remove_dataxy()
    else:
        MODE = 2
        NUMBER_LOOP_CHK = 0
        remove_dataxy()
else:
    ReadExcel()
    if NUMBER_OF_PIC != NUMBER_OF_XY:
        MODE = 2
        NUMBER_LOOP_CHK = 0
        remove_dataxy()
    else:
        flow_screenshot()

inc = 1

key_listener = keyboard.Listener(on_release=on_key)
key_listener.start()
listener_running = True

with mouse.Listener(on_click=on_click) as listener:
    try:
        listener.join()
    except Exception as e:
        print('Done'.format(e.args[0]))
