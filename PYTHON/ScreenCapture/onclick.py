from pynput import keyboard
from pynput import mouse
from pynput.keyboard import Key, Listener
import logging
from datetime import datetime
from log_file import log
import os
import time
import re

# f=open('maniac1.txt','a')
log.info('Press Esc to quit.')
log.info('Press f10 to continue.')
inc = 1
# f.write('<mouse_new>\n')
px = 0
py = 0
loop_perpig = 1
np = 3 
i = 0
posi = ''
position = ''
position_final = ''


def replaseChar(txt):
    re_1 = ' '.join(txt.split())
    re_2 = re_1.split(',')
    txt_re = ''
    for cha in re_2:
        re_3 = ' '.join(cha.split())
        txt_re += re_3+','
    txt_final = txt_re.replace(',,', '')
    return txt_final


def on_functionEsc(key):
    global loop_perpig
    global position
    if (key == keyboard.Key.esc):
        log.info('Esc is pressed ==> stop process')
        listener.stop()
    if (key == keyboard.Key.f10):
        if loop_perpig != 1:
            log.warning('invalid position in picture!')
            log.warning('Pless select last position in display then press \'f10\'')
        else:
            log.info('Success...')
            position_final = replaseChar(position)
            log.info('python run_screenshot.py \'%s\' \n ' % position_final)
            os.system('python run_screenshot.py  \"'+position_final+'\"')
            key_listener.stop()
            listener.stop()


key_listener = keyboard.Listener(on_release=on_functionEsc)
key_listener.start()


def on_move(x, y):
    global loop_perpig
    if loop_perpig == 2:
        positionStr = 'W: ' + str(x-px).rjust(4) + ' H: ' + str(y-py).rjust(4)
        print(positionStr, end='')
        print('\b' * len(positionStr), end='', flush=True)
    else:
        positionStr = 'X: ' + str(x).rjust(4) + ' Y: ' + str(y).rjust(4)
        print(positionStr, end='')
        print('\b' * len(positionStr), end='', flush=True)


def on_click(x, y, button, pressed):
    positionStr = ''
    global posi
    global position
    global i
    global loop_perpig
    global px
    global py

    if button == mouse.Button.left:
        if pressed:
            if loop_perpig == 2:
                px = x-px
                py = y-py

                x = str(px).rjust(4)
                y = str(py).rjust(4)
                # print('xx=',x)
                if int(x) < 0 or int(y) < 0:
                    log.warning('invalid position Width Height in picture!')
                    log.warning(
                        'Pless select Width Height in display again then press \'f10\'')
                else:
                    positionStr = 'W: ' + x + ' H: ' + y
                    position = position+x+' '+y+' '
                    position = position+','
                    loop_perpig = 0
                    i += 1
                    loop_perpig += 1
                    log.info(positionStr)
            else:
                px = x
                py = y
                x = str(px).rjust(4)
                y = str(py).rjust(4)

                if int(x) < 0 or int(y) < 0:
                    log.warning('invalid position X Y in picture!')
                    log.warning(
                        'Pless select X , Y in display again')
                else:
                    positionStr = 'X: ' + x + ' Y: ' + y
                    position = position+x+' '+y+' '

                    i += 1
                    loop_perpig += 1
                    log.info(positionStr)


with mouse.Listener(on_move=on_move, on_click=on_click) as listener:
    try:
        listener.join()
    except Exception as e:
        log.error('Done %s' % format(e.args[0]))
