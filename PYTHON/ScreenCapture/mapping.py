import os
cwd = os.getcwd()

PATH_IMAGE_ORDER = cwd+"\order_img"
PATH_IMAGE_DESTINATION = cwd+"\screenshot"
PATH_EXCEL = cwd+"\excel"
PATH_LOGS = cwd+'\logs'

excel_name = 'DailyOpereitonReport'
sheet_name = 'REPORT'

cell_map = {'cell_img': 'A<X>'}
