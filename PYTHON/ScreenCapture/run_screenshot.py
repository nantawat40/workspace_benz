import pyautogui
import time
import datetime
import sys
import glob
import os
from mapping import PATH_IMAGE_ORDER
from log_file import log

if len(sys.argv) != 2:
    log.info('Invalid parameters: python run_screenshot.py "<x y w h>,<x y w h>" ')
    exit()

screenshotName = "screenshot"
path = PATH_IMAGE_ORDER

if not os.path.exists(path):
    os.makedirs(path)

# print('Number of arguments:', len(sys.argv), 'arguments.')
# print('Argument List:', str(sys.argv))

# print(sys.argv[1])
arg = sys.argv[1]
pig = arg.split(',')
log.info('number of picture : %d' % len(pig))

timer = 0
i = 0
while i < len(pig):
    log.info('\n')
    posi = pig[i].split(' ')

    if len(posi) != 4:
        log.error('invalid parameter (x,y,w,h):%s' % pig[i])
        break

    x_start_point = posi[0]
    y_start_point = posi[1]
    screenWidth = posi[2]
    screenHeight = posi[3]
    chk_screenxy = pyautogui.onScreen(int(x_start_point), int(y_start_point))
    chk_screenwh = pyautogui.onScreen(int(screenWidth), int(screenHeight))

    if not chk_screenxy or not chk_screenwh:
        log.error('invalid parameter (x,y,w,h):%s' % pig[i])
        break

    log.info('x point %s px.'% x_start_point)
    log.info('y point %s px.'% y_start_point)
    log.info('screen_Width %s px.'% screenWidth)
    log.info('screen_Height %s px.'% screenHeight)
    log.info('..................')
    now = datetime.datetime.now()
    now_two_param = str(now).split(" ")
    date = str(now_two_param[0])
    raw_time = str(now_two_param[1]).replace('.', '')
    time_clean = raw_time.replace(':', '')

    try:
        log.info('taking a screenshot...')
        screenshot = pyautogui.screenshot(path + '/' + screenshotName + date + "_" + time_clean +
                                          ".png", region=(x_start_point, y_start_point, screenWidth, screenHeight))
    except Exception as e:
        log.error(e)

    log.info("screenshot taken , parameters of  the file are:%s" %
             str(screenshot))

    files_path = os.path.join(path, '*')
    files = sorted(glob.iglob(files_path), key=os.path.getatime, reverse=True)

    last_screenshot = files[0]
    log.info("Lasted screenshot is: %s" % str(last_screenshot))

    log.info("Going to wait: %s seconds until next cycle" % str(timer))
    i += 1
    time.sleep((int(timer)))

log.info('python move_to_excel.py\n')
os.system('python move_to_excel.py')
