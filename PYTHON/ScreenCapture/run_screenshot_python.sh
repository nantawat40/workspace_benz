#!/bin/bash

#--------------------------------------------------------------------
#              Copyright C - 2012 All rights reserved
#                        G-Able Thailand.
#
#       No part of this program may be reproduced or adapted in any
#             form or by any means, electronic or mechanical,
#       without permission from G-Able Thailand Thailand.
#          This program is confidential and may not be disclosed,
#          decompiled or reverse engineered without permission in
#             writing from G-Able Thailand Thailand.
#
#--------------------------------------------------------------------
# DESCRIPTION:
#    create table/view/object on database on area staging
#
#--------------------------------------------------------------------
# Revision   Date        CR No.   Author    Description
#====================================================================
#
#
#
#
#
#
function pause(){
 read -s -n 1 -p "Press any key to continue . . ."
 echo ""
}

cd D:/Workspace_benz/Mariadb/Framework/PYTHON/ScreenCapture/
python onclick.py

RESULT=$?

if [ $RESULT -ne 0 ]
then

    echo "Terminate error return code [$RESULT]"
    pause
    exit 1
fi
pause


