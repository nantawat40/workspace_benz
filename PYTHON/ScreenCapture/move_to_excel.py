import xlsxwriter
import sys
import glob
import os
import time
import datetime
from mapping import cell_map, excel_name, sheet_name, PATH_IMAGE_ORDER, PATH_IMAGE_DESTINATION, PATH_EXCEL
import pyautogui
import PIL
import shutil
from log_file import log

if not os.path.exists(PATH_IMAGE_DESTINATION):
    os.makedirs(PATH_IMAGE_DESTINATION)
if not os.path.exists(PATH_EXCEL):
    os.makedirs(PATH_EXCEL)

now = datetime.datetime.now()
now_two_param = str(now).split(" ")
date = str(now_two_param[0])
raw_time = str(now_two_param[1]).split(".")
time_raw = str(raw_time[0])
time_clean = time_raw.replace(':', '')


files_path = os.path.join(PATH_IMAGE_ORDER, '*')
files = sorted(glob.iglob(files_path), key=os.path.getatime, reverse=True)
log.info('image in order_img is: %d files.' % len(files))
# exit(0)

# Create an new Excel file and add a worksheet.
excel_filename=excel_name+'_'+date+'.xlsx'
OUTPUT_FILE = PATH_EXCEL+'\\'+excel_filename
log.info('Creating... %s' % excel_filename)
log.info('Write image to excel file please wait.')

# Creat Workbook
workbook = xlsxwriter.Workbook(OUTPUT_FILE)
worksheet = workbook.add_worksheet(sheet_name+date)

# Writer excel
i = 1
for image in files:
    if i == 1:
        worksheet.write('A'+str(i), 'Test Header')
        i += 1
    with PIL.Image.open(image) as im:
        x, y = im.size
    # img_detail = PIL.Image.open(image)
    # width, height = img_detail.size
    width = x
    height = y
    # print(width, height)

    log.info('.')
    cell = cell_map['cell_img'].replace('<X>', str(i))
    worksheet.insert_image(cell, image, {'x_scale': 0.5, 'y_scale': 0.5})
    worksheet.set_row(i-1, (height*0.375)+5)
    i += 1

workbook.close()
log.info('Create file %s successful' % OUTPUT_FILE)
log.info('Move images from %s to %s ' % (str(PATH_IMAGE_ORDER), str(PATH_IMAGE_DESTINATION)))

for image in files:
    img_name = str(image).split('\\')
    img_name_final = img_name[len(img_name)-1]
    # print('O=',image)
    # print('D=',path_image_destination+"\\"+img_name_final)
    shutil.move(image, PATH_IMAGE_DESTINATION+"\\"+img_name_final)
log.info('successfully.\n')
