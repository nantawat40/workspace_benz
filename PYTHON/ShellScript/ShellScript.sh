# Display Message using echo command
#echo "Enter Your Name:\c"
#read name
#echo "Hello $name"

# variable
#a=10
#echo "a = $a"
#a=20
#echo "a = $a"

# rename File text
#echo "Enter file"
#mv $1 $2
#cat $2

# IF-THEN STATEMENT txt
#echo "Input file to rename: \c"
#read source target

#if mv $source $target
#then 
#echo "Rename Success"
#echo $?
#fi

# IF-THEN STATEMENT txt
# echo "Input your number : \c"
# read num
# if [ $num -le 10 ]
# then
# 	echo "Number 10>>>"
# elif [ $num -ge 15 ]
# then
# 	echo "Number 15>>>"
# else 
# 	echo "Not Found"
# fi



# IF-THEN STATEMENT txt
#echo "Input file name : \c"
#read fname
#if [ -f $fname ]
#then
#	if [ -w $fname ]
#	then
#		echo "Write Text to File Ctrl+D Exit"
#		cat>>$fname
#	else
#		echo "You not Permission to write"
#	fi
#fi


# case statement
echo "Enter your Charactor: \c"
read var
case $var in
[a-z])
	echo "Charator is Lowercase"
;;
[A-Z])
	echo "Charator is Uppercase"
;;
[0-9])
	echo "Charator is Numaric"
;;
?)
	echo "Charator is Symbol"
;;
*)
	echo "Charator more then one charactor"
;;
esac

















