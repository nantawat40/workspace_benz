 _logger "-------------------------------" $LOG_FILE
    _logger "Verify Table : $STG_TABLE_NAME" $LOG_FILE
    _logger "-------------------------------" $LOG_FILE

    . $SQL_FILE

    ${DATABASE_CLIENT_COMMAND} --user ${DATABASE_ETL_USERNAME} \
                           --password=${DATABASE_ETL_PASSWORD} \
                            >$RESULT_FILE 2>$ERROR_FILE \
                            --unbuffered --silent --skip-column-names \
                            -e "$SQL_GET_ALL_FIELDS"

    STG_ALL_FIELDS=$(cat $RESULT_FILE)
    CND_WHERE_IS_NULL=$(echo "$STG_ALL_FIELDS" | sed -e 's/,/ IS NULL or /g' -e 's/$/ IS NULL/g')
    CND_WHERE_IS_BLANK=$(echo "$STG_ALL_FIELDS" | sed -e 's/^/(/g' -e 's/,/),(/g' -e 's/$/)/g' | sed -e 's/(/LENGTH(/g' | sed -e 's/),/) = 0 or /g' -e 's/$/ = 0/')

    CND_WHERE="$CND_WHERE_IS_NULL or "$CND_WHERE_IS_BLANK
  
    . $SQL_FILE
  echo "[=== " . $SQL_FILE " ===]"
    ${DATABASE_CLIENT_COMMAND} --user ${DATABASE_ETL_USERNAME} \
                           --password=${DATABASE_ETL_PASSWORD} \
                            >$RESULT_FILE 2>$ERROR_FILE \
                            --unbuffered -t \
    <<EOF                           
    $SQL_SELECT_DATABASE
    \! echo "------------------------"
    \! echo "NUMBER COLUMNS"
    \! echo "------------------------"
    $SQL_COUNT_COLUMNS
    \! echo "------------------------"
    $SQL_COUNT_ROW
    \! echo "------------------------"
    \! echo "SHOW RECORD PER SOURCE"
    \! echo "------------------------"    
    $SQL_VERIFY_COUNT_ROW_STG
    \! echo "------------------------"    
    \! echo "RECORD NULL or BLANK"
    \! echo "------------------------"        
    $SQL_VERIFY_VALUE_NULL
    \! echo "------------------------"
    \! echo "ALL ROW DUPLICATE"
    \! echo "------------------------"    
    $SQL_VERIFY_DUP_ROW_STG
    \q
EOF

   cat $RESULT_FILE

    # remove charecter [space, tab]
    sed 's/=[\t]/ = /g' <$RESULT_FILE >$RESULT_FILE.tmp
    mv $RESULT_FILE.tmp $RESULT_FILE
    
    #. $RESULT_FILE
    # cat $RESULT_FILE
    #_fileToLogger $RESULT_FILE

    _fileToLogger $RESULT_FILE $LOG_FILE

echo "RESULT_FILE = " $RESULT_FILE

    #_logger "TOTAL_RECORD = $TOTAL_RECORD" $LOG_FILE
    #_logger "TOTAL_COLUMNS = $TOTAL_COLUMNS" $LOG_FILE
    _logger "-------------------------------" $LOG_FILE

    _logger "Copy File Out $RESULT_FILE to Test File : $TEST_FILE" $LOG_FILE
    cp $RESULT_FILE $TEST_FILE
    