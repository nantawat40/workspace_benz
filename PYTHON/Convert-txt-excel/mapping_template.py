# Mathod for Mapping
UNT_Summary = {
    'Scenario:': 'A2',
    'Scenario_value': 'B2:C2',
    'Summary_01': 'F2',
    'Summary_02': 'F3',
    'Summary_03': 'F4',
    'Summary_04': 'F5',
    'Summary_05': 'F6',
    'Summary_06': 'F7',
}

UNT_Summary_value = {
    'Scenario:': 'Scenario:',
    'Scenario_value': 'Check loaded data for Curated',
    'Summary_01': 'Passed',
    'Summary_02': 'Passed with Condition',
    'Summary_03': 'Failed',
    'Summary_04': 'Cancelled',
    'Summary_05': 'In Progress',
    'Summary_06': 'Not Start'
}
UNT_Mapping_Head_cell = {
    'SCRIPT_EXECUTION': ['A10', 22],
    'TEST_CATECORY': ['A12', 22],
    'SCENARIO_ID': ['B12', 15],
    'TEST_SCENARIO': ['C12', 40],
    'EXPECTED_RESULT': ['D12', 40],
    'ACTUAL_RESULT': ['E12', 25],
    'PASS_FAIL': ['F12', 25],
    'RESPONSIBLE_TESTER': ['G12', 25],
    'EXECUTE_DATE': ['H12', 25],
    'REMARKS': ['I12', 15],
}


UNT_Mapping_Head_Value = {
    'SCRIPT_EXECUTION': 'SCRIPT EXECUTION:',
    'TEST_CATECORY': 'Test Category',
    'SCENARIO_ID': 'Scenario ID',
    'TEST_SCENARIO': 'Test Scenario',
    'EXPECTED_RESULT': 'Expected Result',
    'ACTUAL_RESULT': 'Actual Result',
    'PASS_FAIL': 'Pass/Fail',
    'RESPONSIBLE_TESTER': 'Responsible Tester',
    'EXECUTE_DATE': 'Execute date',
    'REMARKS': 'Remarks'
}

UNT_result_example = {
    'SCENARIO_ID': ['A<X>', 40],
    'TEST_SCENARIO': ['B<X>', 40],
    'EXPECTED_RESULT': ['C<X>', 40],
    'ACTUAL_RESULT': ['D<X>', 40],
    'PASS_FAIL': ['E<X>', 40],
    'IMAGE_SQL': ['A<X>', 40],
    'IMAGE_RESULT': ['A<X>', 40]
}

UNT_result_example_value = {
    'SCENARIO_ID': 'Scenario ID',
    'TEST_SCENARIO': 'Test Scenario',
    'EXPECTED_RESULT': 'Expected Result',
    'ACTUAL_RESULT': 'Actual Result',
    'PASS_FAIL': 'Pass/Fail',
    'IMAGE_SQL': [],
    'IMAGE_RESULT': []
}
UNT_Mapping_cell = {
    # 'SCRIPT_EXECUTION': 'A<X>',
    'TEST_CATECORY': 'A<X>',
    'SCENARIO_ID': 'B<X>',
    'TEST_SCENARIO': 'C<X>',
    'EXPECTED_RESULT': 'D<X>',
    'ACTUAL_RESULT': 'E<X>',
    'PASS_FAIL': 'F<X>',
    'RESPONSIBLE_TESTER': 'G<X>',
    'EXECUTE_DATE': 'H<X>',
    'REMARKS': 'I<X>'
}

UNT_Mapping_Value = {
    # 'SCRIPT_EXECUTION': 'SCRIPT EXECUTION:',
    'TEST_CATECORY': '',
    'SCENARIO_ID': [],
    'TEST_SCENARIO': [],
    'EXPECTED_RESULT': [],
    'ACTUAL_RESULT': [],
    'PASS_FAIL': [],
    'RESPONSIBLE_TESTER': [],
    'EXECUTE_DATE': [],
    'REMARKS': []
}
UNT_auto_adjust = {
    'adjust_images_sql': [],
    'adjust_images_result': []
}
