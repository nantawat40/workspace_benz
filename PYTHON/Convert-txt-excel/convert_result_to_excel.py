##############################################################################
#
# A simple example of merging cells with the XlsxWriter Python module.
#
# Copyright 2013-2020, John McNamara, jmcnamara@cpan.org
#
##############################################################################
from datetime import datetime
import logging
from PIL import Image, ImageDraw, ImageFont
import glob
import xlsxwriter
import sys
import os
import hashlib
from os import walk
from mapping_template import UNT_Mapping_Head_Value, UNT_Mapping_Head_cell, UNT_Mapping_Value, UNT_Mapping_cell, UNT_Summary, UNT_Summary_value, UNT_result_example, UNT_result_example_value, UNT_auto_adjust
from getTimeFile import creation_date

# check argument
if len(sys.argv) != 5:
    print('Invalid parameters: convert_result_to_excel.py -p <PATH_RESULT> -b <BATCH NUMBER>')
    print('example : convert_result_to_excel.py -p \"output/tests/unit_test\" -b \"01\"')
    exit()

PATH_RESULT = ''
BATCH_NO = ''

a = 0
for i in sys.argv:
    if i == '-p':
        PATH_RESULT = sys.argv[a+1]
    if i == '-b':
        BATCH_NO = sys.argv[a+1]
    a += 1

# initialize path
# MAIN_PATH = "output/tests"
INPUT_PATH = PATH_RESULT+'/'+BATCH_NO
OUTPUT_PATH = INPUT_PATH
IMAGE_PATH = PATH_RESULT+'/images/'+BATCH_NO
LOGS_PATH = PATH_RESULT+'/logs'

if not os.path.exists(IMAGE_PATH):
    os.makedirs(LOGS_PATH)
    
if not os.path.exists(LOGS_PATH):
    os.makedirs(LOGS_PATH)


# datetime object containing current date and time
now = datetime.now()
dt_string = now.strftime("%Y-%m-%d")

# set up logging to file - see previous section for more details
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    filename=LOGS_PATH+'/'+dt_string+'.log')

# define a Handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler()
console.setLevel(logging.INFO)
# set a format which is simpler for console use
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
# tell the handler to use this format
console.setFormatter(formatter)
# add the handler to the root logger
logging.getLogger('').addHandler(console)

# Now, we can log to the root logger, or any other logger. First the root...
logging.info('Strat Logs')

# Now, define a couple of other loggers which might represent areas in your
logger = logging.getLogger('myapp')


# initialize variables
NUM_OF_RULE = 1
# NUM_OF_TRUE = 0
# NUM_OF_FALSE = 0
TABLE_NAME, TABLE_TYPE, RULE_NAME, RULE_DESC, RULE_RESULT, RULE_STATUS = '', '', '', '', '', ''
# STATUS_OF_TEST = 'TRUE'
FILE_INPUT = ''


# Method get all files form 'INPUT_PATH'


def get_list_files(input_path):
    logger.info("get list files on Input path")
    list_files = []
    # os.chdir(path)
    for file in glob.glob(input_path+"/*.result"):
        list_files.append(file)
    return list_files

# Method check Directory exists


def check_dir(path):
    if not os.path.exists(path):
        return False
    return True

# Method check file exists


def check_file(file):
    if not os.path.isfile(file):
        logger.error("No such file or directory: \'%s\' \n", file)
        return False
    return True


def setColumn(collumn):
    if not isinstance(collumn, list):
        if "<X>" not in collumn:
            if ":" not in collumn:
                set_col = collumn+":"+collumn
            else:
                set_col = collumn
            return set_col


def createImage(image_name, rule_name, content, status):
    bg_color = '#EEEEEE'  # Light gray
    font_color = '#000000'  # black
    hight_width = []
    if check_dir(IMAGE_PATH) is False:
        os.makedirs(IMAGE_PATH)
    img_name = IMAGE_PATH+'/'+image_name+'_'+rule_name+'.png'
    path_, file_img_name = os.path.split(img_name)
    logger.info("Create Images : %s", file_img_name)

    # # cheang color
    if "FALSE" in status or "Failed" in status:
        # default color (Status of TRUE")
        bg_color = '#FFE700'  # yellow
        font_color = '#FF0000'  # red

    # use a bitmap font
    font = ImageFont.truetype("arial.ttf", 15)
    w, h = font.getsize_multiline(content)
    hight_width.append(h+50)
    hight_width.append(w+50)
    if image_name.find('SQL_') == 0:
        UNT_auto_adjust['adjust_images_sql'].append(hight_width)
    else:
        UNT_auto_adjust['adjust_images_result'].append(hight_width)
    # print ('w=',w,"h=",h)
    image = Image.new('RGB', (w+50, h+50), bg_color)
    draw = ImageDraw.Draw(image)
    draw.text((10, 10), content, font=font, fill=font_color)
    image.save(img_name)
    return img_name


def get_txt_from_file(file_input):
    logger.info('Starting get content from text file')
    global NUM_OF_RULE
    global TABLE_NAME, TABLE_TYPE, RULE_NAME, RULE_DESC, RULE_RESULT, RULE_STATUS
    global FILE_INPUT

    NUM_OF_RULE = 1
    TABLE_NAME, TABLE_TYPE, RULE_NAME, RULE_DESC, RULE_RESULT, RULE_STATUS = '', '', '', '', '', ''
    FILE_INPUT = ''

    is_sql = False
    is_result = False
    is_num = 0
    sql_string = ''
    result_string = ''
    for line in open(file_input):
        resule_group = []

        if "TABLE NAME" in line:
            TABLE_NAME = ((line.split('and')[0]).split(
                '=')[1]).replace(' ', '')
            TABLE_TYPE = ((line.split('and')[1]).split(
                '=')[1]).replace('\n', '')
            logger.info("TABLE_NAME : %s", TABLE_NAME)
            logger.info("TABLE_TYPE : %s", TABLE_TYPE)
            UNT_Mapping_Value['TEST_CATECORY'] = TABLE_TYPE
        if "Result of SQL :" in line:
            # print(line)
            is_sql = True
            is_num = 1
        if is_sql is True and is_num == 1:
            # print("yes1")
            if "--------------" in line:
                # print("yes2")
                is_num = 0
        if is_sql is True and is_num == 0:
            # print(line)
            sql_string += line
            if "-------------------------------" in line or "+--" in line:
                sql = sql_string.split('--------------')[1]
                img_name = createImage("SQL_"+TABLE_NAME, (RULE_NAME.split(
                    'STG_')[1]).replace(' ', ''), sql, RULE_STATUS)
                UNT_result_example_value['IMAGE_SQL'].append(img_name)
                logger.info('STATEMENT_OF_SQL : ...')
                sql_string = ''
                is_sql = False

        if "RULE" in line:
            if "_"+str(NUM_OF_RULE) in line:
                if "RESULT" in line:
                    if "TRUE" in line:
                        RULE_STATUS = 'Passed'
                    else:
                        RULE_STATUS = 'Failed'
                    if "and" in line:
                        img_name = createImage(TABLE_NAME, (RULE_NAME.split(
                            'STG_')[1]).replace(' ', ''), line, RULE_STATUS)
                        UNT_result_example_value['IMAGE_RESULT'].append(
                            img_name)

                        RULE_RESULT = (line.split('RESULT =')
                                       [1]).replace('\n', '')
                    else:
                        RULE_RESULT = (line.split('=')[1]).replace('\n', '')
                    UNT_Mapping_Value['SCENARIO_ID'].append(NUM_OF_RULE)
                    NUM_OF_RULE += 1

                    UNT_Mapping_Value['TEST_SCENARIO'].append(RULE_DESC)
                    UNT_Mapping_Value['EXPECTED_RESULT'].append(
                        "Expected Result")
                    UNT_Mapping_Value['ACTUAL_RESULT'].append("Actual Result")
                    UNT_Mapping_Value['PASS_FAIL'].append(RULE_STATUS)
                    UNT_Mapping_Value['RESPONSIBLE_TESTER'].append(
                        "<Who are testing>")
                    UNT_Mapping_Value['EXECUTE_DATE'].append(now)
                    UNT_Mapping_Value['REMARKS'].append("REMARKS")
                else:
                    RULE_NAME = line.split('(')[0]
                    RULE_DESC = (
                        (line.split('(')[1]).replace(')', '')).replace('\n', '')
                    logger.info('TEST_SCENARIO  : %s', RULE_DESC)
        if "+---" in line:
            is_result = True
        if is_result is True:
            if not ": RESULT =" in line:
                result_string += line
            else:
                result_string += line
                is_result = False
                img_name = createImage(TABLE_NAME, (RULE_NAME.split('STG_')
                                                    [1]).replace(' ', ''), result_string, RULE_STATUS)
                UNT_result_example_value['IMAGE_RESULT'].append(img_name)
                result_string = ''


def clear_values():
    for variable in UNT_Mapping_Value:
        if isinstance(UNT_Mapping_Value[variable], list):
            UNT_Mapping_Value[variable].clear()
    for variable in UNT_auto_adjust:
        if isinstance(UNT_auto_adjust[variable], list):
            UNT_auto_adjust[variable].clear()
    for variable in UNT_result_example_value:
        if isinstance(UNT_result_example_value[variable], list):
            UNT_result_example_value[variable].clear()


# Method write .xlsx by library excelwriter
def excelwriter(table_name):

    # Create an new Excel file and add a worksheet.
    OUTPUT_FILE = OUTPUT_PATH+'/'+TABLE_NAME+'.xlsx'
    workbook = xlsxwriter.Workbook(OUTPUT_FILE)
    worksheet = workbook.add_worksheet('Scenario')
    worksheet_result = workbook.add_worksheet('Result (Example)')
    # worksheet_detail = workbook.add_worksheet('Result_Detail')

    # Create a format to use in the merged range.
    fmt_H1 = workbook.add_format({
        'font_size': 10,
        'font_name': 'Tahoma',
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#0066CC',
        'font_color': 'white',
        'text_wrap': True})

    fmt_False = workbook.add_format({
        'font_size': 12,
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'red',
        'font_color': 'white'})

    fmt_True = workbook.add_format({
        'font_size': 12,
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'green',
        'font_color': 'white'})

    fmt_False_left = workbook.add_format({
        'font_size': 12,
        'bold': 1,
        'border': 1,
        'align': 'left',
        'valign': 'vcenter',
        'fg_color': 'red',
        'font_color': 'white'})

    fmt_True_left = workbook.add_format({
        'font_size': 12,
        'bold': 1,
        'border': 1,
        'align': 'left',
        'valign': 'vcenter',
        'fg_color': 'green',
        'font_color': 'white'})

    fmt_HC2 = workbook.add_format({
        'font_size': 12,
        'bold': 12,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#d9d9d9',
        'font_color': 'black'})

    fmt_H2 = workbook.add_format({
        'font_size': 12,
        'bold': 12,
        'border': 1,
        'align': 'left',
        'valign': 'vcenter',
        'fg_color': '#d9d9d9',
        'font_color': 'black'})

    fmt_result_bold = workbook.add_format({
        'font_size': 10,
        'font_name': 'Tahoma',
        'bold': 1,
        'border': 1,
        'align': 'left',
        'valign': 'vcenter',
        'fg_color': 'white',
        'font_color': 'black',
        'text_wrap': True})  # 'num_format': 'dd/mm/yyyy hh:mm'

    fmt_result_true = workbook.add_format({
        'font_size': 12,
        'border': 1,
        'align': 'left',
        'valign': 'vcenter',
        'fg_color': 'white',
        'font_color': 'black',
        'text_wrap': True})

    fmt_result = workbook.add_format({
        'font_size': 10,
        'font_name': 'Tahoma',
        'border': 1,
        'align': 'left',
        'valign': 'vcenter',
        'fg_color': 'white',
        'font_color': 'black',
        'text_wrap': True})

    fmt_result_fill_f = workbook.add_format({
        'font_size': 10,
        'font_name': 'Tahoma',
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'yellow',
        'font_color': 'red',
        'text_wrap': True})

    fmt_result_fill_t = workbook.add_format({
        'font_size': 12,
        'border': 1,
        'align': 'left',
        'valign': 'vcenter',
        'fg_color': 'white',
        'font_color': 'green'})

    fmt_DateTime = workbook.add_format({
        'num_format': 'dd/mm/yy hh:mm',
        'font_size': 12,
        'border': 1,
        'align': 'left',
        'valign': 'vcenter',
        'fg_color': 'white',
        'font_color': 'red'
    })

    # start writer to cell
    # Header (Merge cell)
    for variable in UNT_Mapping_Head_cell:
        if "<X>" not in UNT_Mapping_Head_cell[variable][0]:
            if ":" in UNT_Mapping_Head_cell[variable][0]:
                worksheet.merge_range(
                    UNT_Mapping_Head_cell[variable][0], UNT_Mapping_Head_Value[variable], fmt_H1)
            else:
                worksheet.write(UNT_Mapping_Head_cell[variable][0],
                                UNT_Mapping_Head_Value[variable], fmt_H1)
                set_col = setColumn(UNT_Mapping_Head_cell[variable][0])
                worksheet.set_column(
                    set_col, UNT_Mapping_Head_cell[variable][1])

    # Result other (Merge cell)
    # print("NUM_OF_RULE", NUM_OF_RULE)
    last_row = 0
    for variable in UNT_Mapping_cell:
        last_row = int(
            (UNT_Mapping_Head_cell['TEST_CATECORY'][0]).replace('A', ''))
        if "<X>" in UNT_Mapping_cell[variable]:
            # print("this = ", UNT_Mapping_cell[variable])

            if isinstance(UNT_Mapping_Value[variable], list):
                for lists in range(NUM_OF_RULE-1):
                    last_row += 1
                    cell = (UNT_Mapping_cell[variable]).replace(
                        "<X>", str(last_row))
                    # print(cell, UNT_Mapping_Value[variable][lists])
                    if "EXECUTE_DATE" in variable or "RESPONSIBLE_TESTER" in variable:
                        worksheet.write(
                            cell, UNT_Mapping_Value[variable][lists], fmt_DateTime)
                    else:
                        if "ACTUAL_RESULT" in variable or "PASS_FAIL" in variable:
                            worksheet.write(
                                cell, UNT_Mapping_Value[variable][lists], fmt_result_bold)
                        else:
                            worksheet.write(
                                cell, UNT_Mapping_Value[variable][lists], fmt_result)
            else:
                last_row += 1
                cell = (UNT_Mapping_cell[variable]).replace(
                    "<X>", str(last_row))
                # print(cell, UNT_Mapping_Value[variable])
                worksheet.write(cell, UNT_Mapping_Value[variable], fmt_result)

    for variable in UNT_Summary:
        if "Scenario" in variable:
            worksheet.write(UNT_Summary[variable],
                            UNT_Summary_value[variable], fmt_result_bold)
            if ":" in UNT_Summary[variable]:
                worksheet.merge_range(
                    UNT_Summary[variable], UNT_Summary_value[variable], fmt_result)
        else:
            worksheet.write(UNT_Summary[variable],
                            UNT_Summary_value[variable], fmt_result_bold)
            value_cell = UNT_Summary[variable].replace('F', 'G')

            data = "=COUNTIF($F$11:$F"+str(last_row) + \
                ","+UNT_Summary[variable]+")"
            worksheet.write(
                value_cell, data, fmt_result)

    num_row = 0
    for rulenum in range(NUM_OF_RULE - 1):
        num_row += 1

        # Header
        for variable in UNT_result_example:
            cell = UNT_result_example[variable][0]
            if "<X>" in cell:
                if "IMAGE" not in variable:
                    # Header
                    actual_row_head = cell.replace('<X>', str(num_row))
                    worksheet_result.write(
                        actual_row_head, UNT_result_example_value[variable], fmt_H1)

                    set_col = actual_row_head+":"+actual_row_head
                    worksheet_result.set_column(
                        set_col, UNT_result_example[variable][1])

        num_row += 1
        # Result
        for variable in UNT_result_example:
            cell = UNT_result_example[variable][0]
            if "<X>" in cell:
                if "IMAGE" not in variable:
                    actual_row = cell.replace('<X>', str(num_row))
                    # Result
                    if "ACTUAL_RESULT" in variable or "PASS_FAIL" in variable:
                        if UNT_Mapping_Value[variable][rulenum].find("Failed") == 0:
                            worksheet_result.write(
                                actual_row, UNT_Mapping_Value[variable][rulenum], fmt_result_fill_f)
                        else:
                            worksheet_result.write(
                                actual_row, UNT_Mapping_Value[variable][rulenum], fmt_result_bold)
                    else:
                        worksheet_result.write(
                            actual_row, UNT_Mapping_Value[variable][rulenum], fmt_result)

        num_row += 2
        # Insert Images IMAGE_SQL
        for variable in UNT_result_example:
            cell = UNT_result_example[variable][0]
            if "<X>" in cell:
                # Insert Images
                if "IMAGE_SQL" in variable:
                    # Image SQL
                    imgsql_row = UNT_result_example['IMAGE_SQL'][0].replace(
                        '<X>', str(num_row))
                    worksheet_result.insert_image(
                        imgsql_row, UNT_result_example_value['IMAGE_SQL'][rulenum], {'x_scale': 0.8, 'y_scale': 0.8})

                    h = UNT_auto_adjust['adjust_images_sql'][rulenum][0]
                    w = UNT_auto_adjust['adjust_images_sql'][rulenum][1]
                    # convert pixels to point scale *** 1 px. == 0.60 point ***
                    h_point = (h*0.60)
                    # convert pixels to point scale *** 1 px. == 0.60 point ***
                    w_point = (w*0.60)

                    # "409" is a Max Point row height
                    MaxPoint = 409
                    if h_point > MaxPoint:
                        worksheet_result.set_row(num_row, MaxPoint)
                        diff_1 = h_point - MaxPoint
                        cell_plus = UNT_result_example['IMAGE_SQL'][0].replace(
                            '<X>', str(num_row+2))

                        worksheet_result.merge_range(cell+":"+cell_plus, '')
                        num_row += 1
                        worksheet_result.set_row(num_row, diff_1)
                    else:
                        worksheet_result.set_row(num_row-1, h_point)

        num_row += 2
        # Insert Images IMAGE_RESULT
        for variable in UNT_result_example:
            cell = UNT_result_example[variable][0]
            if "<X>" in cell:
                # Insert Images
                if "IMAGE_RESULT" in variable:
                    # Image _RESULT
                    imgsql_row = UNT_result_example['IMAGE_RESULT'][0].replace(
                        '<X>', str(num_row))
                    worksheet_result.insert_image(
                        imgsql_row, UNT_result_example_value['IMAGE_RESULT'][rulenum], {'x_scale': 0.8, 'y_scale': 0.8})

                    h = UNT_auto_adjust['adjust_images_result'][rulenum][0]
                    w = UNT_auto_adjust['adjust_images_result'][rulenum][1]
                    # convert pixels to point scale *** 1 px. == 0.60 point ***
                    h_point = (h*0.60)
                    # convert pixels to point scale *** 1 px. == 0.60 point ***
                    w_point = (w*0.60)

                    # "409" is a Max Point row height
                    MaxPoint = 409
                    if h_point > MaxPoint:
                        worksheet_result.set_row(num_row, MaxPoint)
                        diff_1 = h_point - MaxPoint
                        cell_plus = UNT_result_example['IMAGE_RESULT'][0].replace(
                            '<X>', str(num_row+2))
                        worksheet_result.merge_range(
                            imgsql_row+":"+cell_plus, '')
                        num_row += 1
                        worksheet_result.set_row(num_row, diff_1)
                    else:
                        worksheet_result.set_row(num_row-1, h_point)
        num_row += 1

    workbook.close()
    logger.info('Create excel file : %s \n', OUTPUT_FILE)


# chack input path exists
# sys.exit() program when return 'False'
if check_dir(INPUT_PATH) is False:
    logger.error(
        'The system cannot find the directory specified: \'%s\' \n', INPUT_PATH)
    # print(massage)
    sys.exit()

# chack output path exists
# auto create output directory when return 'False'
if check_dir(OUTPUT_PATH) is False:
    os.makedirs(OUTPUT_PATH)

# get all file from INPUT_PATH directory
list_files = get_list_files(INPUT_PATH)
file_no = 1
file_all = len(list_files)
# show total number of files
logger.info('file processing of : %s fils', file_all)


# Start processing (per file)s
for file_name in list_files:
    FILE_INPUT = file_name

    # Verify that the file exists.
    # sys.exit() program when return 'False'
    if check_file(FILE_INPUT) is False:
        sys.exit()

    # Show file being processed
    clear_values()
    path, filename = os.path.split(FILE_INPUT)
    logger.info('Processing file : %s [%s/%s]', filename, file_no,  file_all) 
    FILE_RESULT_TEST = max(FILE_INPUT.split('/'))
    # UNT_Mapping_Value['SCENARIO_ID'] = BATCH_NO

    # call get_txt_from_file Method
    get_txt_from_file(FILE_INPUT)

    # call excelwriter Method
    excelwriter(TABLE_TYPE)
    file_no += 1
