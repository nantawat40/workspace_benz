import pandas as pd
print("Pandas version", pd.__version__)
input = 'INPUT/JOB_MASTER.csv'
# Read DF
#df = pd.read_csv(input)
# Sometimes reading CSV for Excel need encoding
df = pd.read_csv(input, encoding="ISO-8859-1")
# Print head and tail
#print (df.head())
#print (df.tail())
#print (df.sample())
#print (df.dtypes)
#df['JOB_NAME'] = pd.to_numeric(df['JOB_NAME'])
#print (df.describe())

#วิธีเช็ค Summary (count, min, max, mean) แบบแยกกลุ่ม
test = df.groupby(['JOB_ID'])
#print (test.describe())

#สร้าง DataFrameใหม่
dataframe = pd.DataFrame({
    'C1': pd.date_range('20170101', periods=4),
    'C2' : [10,20,30,40],
    'C3': pd.Categorical(['A','B','C','D']),
    'C4': 1})
print (dataframe)

import numpy
array = numpy.array([(1,2,3), (4,None,6),(7,8,None)])
dataframe = pd.DataFrame(array,columns=['C1','C2','C3'])
print (dataframe)

#วิธีเลือกหลายคอลัมน์จาก DataFrame
print (dataframe[['C1','C2']])

#วิธีเลือกคอลัมน์ตามเงื่อนไขที่ต้องการ
print("วิธีเลือกคอลัมน์ตามเงื่อนไขที่ต้องการ")
#บางทีเราอยาก Filter เฉพาะคอลัมน์ที่มีค่าตามที่เราต้องการโดยใช้ .loc ได้ โดยสามารถเลือก Filter แบบ .all() (ทุกค่าในคอลัมน์ต้องตรงตามเงื่อนไข) หรือ .any() (บางค่าในคอลัมน์ต้องตรงตามเงื่อนไข)
dataframe2 = dataframe.loc[:,dataframe.notnull().all()]
dataframe3 = dataframe.loc[:,(dataframe>50).any()] 
print ("K",dataframe2)

#วิธีเลือกแถวตามเงื่อนไขที่ต้องการ
print("วิธีเลือกแถวตามเงื่อนไขที่ต้องการ")
print (dataframe[dataframe['C1']>5])  # เงื่อนไขแบบง่าย ๆ
print (dataframe.loc[dataframe.C1.isin([1,2,3])]) # เงื่อนไขแบบซับซ้อน
print("ถ้ามีหลายเงื่อนไขเราสามารถใช้ & (and) หรือ | (or) ได้")
print(dataframe[(dataframe['C1']>5) & ((dataframe['C2']<2) | (dataframe['C2']>5))])

print("หรือใช้ Query เป็นเงื่อนไขได้ด้วย มีประโยชน์มากเวลาเรามีเงื่อนไขแปลก ๆ ไม่ต้องเขียนลูปขึ้นมาเองเลยครับ")
print (dataframe)
print(dataframe.query('C1 < C3'))