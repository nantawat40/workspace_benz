# echo "Enter result file: "
# read RESULT_FILE
RESULT_FILE="unit_test\unit_test_TABLE_STG_COUNTY_YUL.result"
#unit_test_TABLE_STG_COUNTY_YUL.result
echo "FILE=$RESULT_FILE"
TABLE_NAME=$(grep "STG_COUNTY_YUL" $RESULT_FILE | awk -F'=' '{print $2}' | awk -F'and' '{print $1}')
TABLE_TYPE=$(grep "STG_COUNTY_YUL" $RESULT_FILE | awk -F'=' '{print $3}')
RULE_NAME=$(grep "RULE" $RESULT_FILE | grep "_1" | grep -v "RESULT =" | awk -F'(' '{print $1}')
RULE_DESC=$(grep "RULE" $RESULT_FILE | grep "_1" | grep -v "RESULT =" | awk -F'(' '{print $2}' | sed 's/)//g')
RULE_RESULT=$(grep "STG STG_RULE_ID_1" $RESULT_FILE | grep -v "List : List All Field's LIST" | awk -F'=' '{print $2}')
NUM_OF_RULE=$(grep "RULE" $RESULT_FILE | grep -v "RESULT" | wc -l)
NUM_OF_TRUE=$(grep "RULE" $RESULT_FILE | grep "RESULT" | grep "TRUE" | wc -l)
NUM_OF_FALSE=$(grep "RULE" $RESULT_FILE | grep "RESULT" | grep "TRUE" | wc -l)
# TABLE NAME=STG_COUNTY_YUL and TYPE=STG

 
for i in $(seq 1 $NUM_OF_RULE) ; do 
 RULE_NAME=$(grep "RULE" $RESULT_FILE | grep "_$i" | grep -v "RESULT =" | awk -F'(' '{print $1}')
 echo $RULE_NAME
 done

echo "$TABLE_NAME"
echo "$TABLE_TYPE"
echo "$RULE_NAME"
echo "$RULE_DESC"
echo "$RULE_RESULT"
echo "$NUM_OF_RULE"
