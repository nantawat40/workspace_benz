##############################################################################
#
# A simple example of merging cells with the XlsxWriter Python module.
#
# Copyright 2013-2020, John McNamara, jmcnamara@cpan.org
#
import xlsxwriter
import pandas as pd

H1 = "UNIT TEST"
H2 = "RULE"
H3 = "RULE DETAIL"
RULE_DETAIL = ['RULE NAME', 'RULE DESCRIPTION',
               'STATEMENT OF SQL', 'RESULT OF SQL', 'RESULT STATUS']

# Create an new Excel file and add a worksheet.
workbook = xlsxwriter.Workbook('C:\\Users\\Admin\\Desktop\\merge1.xlsx')
worksheet = workbook.add_worksheet() 

# Create a format to use in the merged range.
fmt_H1 = workbook.add_format({
    'font_size': 20,
    'bold': 1,
    'border': 1,
    'align': 'center',
    'valign': 'vcenter',
    'fg_color': 'blue',
    'font_color': 'white'})

fmt_False = workbook.add_format({
    'font_size': 12,
    'bold': 1,
    'border': 1,
    'align': 'center',
    'valign': 'vcenter',
    'fg_color': 'red',
    'font_color': 'white'})

fmt_True = workbook.add_format({
    'font_size': 12,
    'bold': 1,
    'border': 1,
    'align': 'center',
    'valign': 'vcenter',
    'fg_color': 'green',
    'font_color': 'white'})


fmt_False_left = workbook.add_format({
    'font_size': 12,
    'bold': 1,
    'border': 1,
    'align': 'left',
    'valign': 'vcenter',
    'fg_color': 'red',
    'font_color': 'white'})

fmt_True_left = workbook.add_format({
    'font_size': 12,
    'bold': 1,
    'border': 1,
    'align': 'left',
    'valign': 'vcenter',
    'fg_color': 'green',
    'font_color': 'white'})


fmt_H2 = workbook.add_format({
    'font_size': 12,
    'bold': 12,
    'border': 1,
    'align': 'center',
    'valign': 'vcenter',
    'fg_color': '#d9d9d9',
    'font_color': 'black'})

fmt_result_bold = workbook.add_format({
    'font_size': 12,
    'bold': 12,
    'border': 1,
    'align': 'left',
    'valign': 'vcenter',
    'fg_color': 'white',
    'font_color': 'red'})

fmt_result = workbook.add_format({
    'font_size': 12,
    'border': 1,
    'align': 'left',
    'valign': 'vcenter',
    'fg_color': 'white',
    'font_color': 'red'})


def get_col_widths(dataframe):
    # First we find the maximum length of the index column
    idx_max = max([len(str(s)) for s in dataframe.index.values] +
                  [len(str(dataframe.index.name))])
    # Then, we concatenate this to the max of the lengths of column name and its values for each column, left to right
    return [idx_max] + [max([len(str(s)) for s in dataframe[col].values] + [len(col)]) for col in dataframe.columns]


# Merge 3 cells.
#worksheet.merge_range('A1:E1', H1, merge_format_H1)
worksheet.merge_range('A1:E1', H1, fmt_H1)
worksheet.merge_range('A5:E5', H2, fmt_H1)
worksheet.merge_range('A9:E9', H3, fmt_H1)

worksheet.write('A2', 'TABLE NAME', fmt_H2)
worksheet.write('A3', 'TYPE', fmt_H2)
worksheet.write('A4', 'STATUS OF TEST', fmt_H2)
worksheet.write('A6', 'NUM OF RULE', fmt_H2)
worksheet.write('A7', 'TRUE', fmt_H2)
worksheet.write('A8', 'FALSE', fmt_H2)

for rownum, data in enumerate(RULE_DETAIL):
    worksheet.write(9, rownum, data, fmt_H2)

"""
Create cells based on the test results
"""

worksheet.merge_range('B2:E2', 'STG_MASTER', fmt_result_bold)
worksheet.merge_range('B3:E3', 'STG', fmt_result_bold)
worksheet.merge_range('B4:E4', 'FALSE', fmt_False_left)

worksheet.merge_range('B6:E6', '6', fmt_result_bold)
worksheet.merge_range('B7:E7', '4', fmt_True_left)
worksheet.merge_range('B8:E8', '2', fmt_False_left)


# Simulate autofit column in xslxwriter
# dataframe = pd.read_excel(worksheet)





result_detail = [["STG STG_RULE_ID_1", "List : List All Field\'s LIST", "Select * from ... ", "+-----+-----+"],
                 ["STG STG_RULE_ID_2",
                     "List : Show Number of Record(s)\'s LIST", "Select * from ... ", "+-----+-----+"],
                 ["STG STG_RULE_ID_3",
                     "List : TOP 100 Record(s)'s LIST", "Select * from ... ", "+-----+-----+"],
                 ["STG STG_RULE_ID_4", "Check : Duplicate Record's CHECK",
                     "Select * from ... ", "count= 9"],
                 ["STG STG_RULE_ID_5", "Check : Null value on all fields's CHECK",
                     "Select * from ... ", "record count = 0"],
                 ["STG STG_RULE_ID_6", "Check : Blank value on all fields's CHECK", "Select * from ... ", "record count = 13"]]
result_status = ['TRUE', 'TRUE', 'TRUE', 'FALSE', 'TRUE', 'FALSE']
# for extact outer array
for loop, array_data in enumerate(result_detail):
    # for extact data in sub array
    for i, data in enumerate(array_data):
        worksheet.write(10+loop, i, data, fmt_result)

for i, data in enumerate(result_status):
    if data == "TRUE":
        worksheet.write(10+i, 4, data, fmt_True)
    else:
        worksheet.write(10+i, 4, data, fmt_False)



#Set wide column
worksheet.set_column('A1:A1', 25)
worksheet.set_column('B1:D1', 45)
worksheet.set_column('E1:E1', 20)
for row in range(1, 4):
    worksheet.set_row(row, 18)
for row in range(5, 8):
    worksheet.set_row(row, 18) 
workbook.close() 