from pathlib import Path
import pandas as pd
import re 

# initial directory variable
INPUT_PATH = "INPUT"
OUTPUT_PATH = "OUTPUT"

Path(INPUT_PATH).mkdir(parents=True, exist_ok=True)
Path(OUTPUT_PATH).mkdir(parents=True, exist_ok=True)

# initial files variable
INPUT_FILE = INPUT_PATH + '/unit_test_TABLE_STG_REPO_MASTER.result'
OUTPUT_FILE = OUTPUT_PATH + 'C:/Users/Admin/Desktop/try.xlsx'

fin = open(INPUT_FILE, "rt")
fout_trans = open("tf_01.txt", "wt")
TRANS_FILE = 'tf_01.txt' 


def getSearchReplace(content):
    fmtre = re.search('[+].*[+]', content)

    if fmtre is None:
        fmtre = re.search('[-].*[-]', content)
        if fmtre is None:
            return None
    grps = fmtre.group(0)
    return grps


# replece txt
fin = open(INPUT_FILE, "rt")
fout_trans = open("tf_01.txt", "wt")

# loop replece txt
for line in fin:
    fmt_replace = getSearchReplace(line.replace("\n", "1"))
#       print (fmt_replace)

    if fmt_replace is not None:
        #         print (fmt_replace)
        # print re.search(r"+--.*--+", line).group())
        fout_trans.write(line.replace("\n", "").replace(fmt_replace, ''))
    else:
        fout_trans.write(line)

fin.close()
fout_trans.close()


def highlight_max(x):
    return ['background-color: yellow' if v == x.max() else ''
            for v in x]

# def row_colour(row):
#    return ['background-color:'+row.colour.lower()for i in row]


# convert .txt to .xlsx
#df = pd.read_csv(TRANS_FILE, sep="|", names=["Sequence", "Start", "End", "Coverage"])
df = pd.read_csv(TRANS_FILE, sep="|", index_col=0, error_bad_lines=False)
# df['Column'] = df[df.columns[0:]].apply(
#       lambda x: ','.join(x.dropna().astype(str)),
#    axis=1
# )
#df.DataFrame(np.random.randn(5, 2))
# df.style.format("{:.10%}")
# df.style.apply(highlight_max)
#df.style.format({'AAAAA': str.lower})
#writer = pd.ExcelWriter('try.xlsx', engine='xlsxwriter')
# df.head(200).style.highlight_min(color='#cd4f39').applymap(lambda x: f"color: {'red' if isinstance(x,str) else 'black'}")
df.to_excel('try.xlsx', engine='xlsxwriter')
