
# Create a format to use in the merged range.
fmt_H1 = workbook.add_format({
    'font_size': 18,
    'bold': 1,
    'border': 1,
    'align': 'center',
    'valign': 'vcenter',
    'fg_color': 'blue',
    'font_color': 'white'})

fmt_False = workbook.add_format({
    'font_size': 12,
    'bold': 1,
    'border': 1,
    'align': 'center',
    'valign': 'vcenter',
    'fg_color': 'red',
    'font_color': 'white'})

fmt_True = workbook.add_format({
    'font_size': 12,
    'bold': 1,
    'border': 1,
    'align': 'center',
    'valign': 'vcenter',
    'fg_color': 'green',
    'font_color': 'white'})


fmt_False_left = workbook.add_format({
    'font_size': 12,
    'bold': 1,
    'border': 1,
    'align': 'left',
    'valign': 'vcenter',
    'fg_color': 'red',
    'font_color': 'white'})

fmt_True_left = workbook.add_format({
    'font_size': 12,
    'bold': 1,
    'border': 1,
    'align': 'left',
    'valign': 'vcenter',
    'fg_color': 'green',
    'font_color': 'white'})


fmt_H2 = workbook.add_format({
    'font_size': 12,
    'bold': 12,
    'border': 1,
    'align': 'center',
    'valign': 'vcenter',
    'fg_color': '#d9d9d9',
    'font_color': 'black'})

fmt_result_bold = workbook.add_format({
    'font_size': 12,
    'bold': 12,
    'border': 1,
    'align': 'left',
    'valign': 'vcenter',
    'fg_color': '#d9d9d9',
    'font_color': 'red'})

fmt_result = workbook.add_format({
    'font_size': 12,
    'border': 1,
    'align': 'left',
    'valign': 'vcenter',
    'fg_color': '#d9d9d9',
    'font_color': 'red'})
