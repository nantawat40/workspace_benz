import pandas as pd

my_team = pd.DataFrame(
    [['Messi', 'Barcelona', 'Forward', 'Arg', 10],
     ['Ronaldo', 'Real Madrid', 'Forward', 'Por', 7],
        ['Pogba', 'Manchester United', 'Midfield', 'Fra', 6],
        ['Suarez', 'Barcelona', 'Forward', 'Urg', '9'],
        ['David de Gea', 'Manchester United', 'Goal Keeper', 'Spa', 1]], index=['Player 1', 'Player 2', 'Player 3', 'Player 4', 'Player 5'], columns=['Name', 'Team', 'Position', 'Country', 'Number']
)

CR7 = my_team.iloc[1]
#print (CR7)

# Create NewTeam
newTeam = my_team.iloc[[0, 1], [0, 1, 4]]
print("New Team")
print(newTeam)
print("#########  TEST MERGE  ##########")
merge = newTeam.merge(newTeam[0,1])
print (merge)
