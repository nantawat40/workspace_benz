package com.example.againsprintboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgainsprintbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(AgainsprintbootApplication.class, args);
         
    }

}
