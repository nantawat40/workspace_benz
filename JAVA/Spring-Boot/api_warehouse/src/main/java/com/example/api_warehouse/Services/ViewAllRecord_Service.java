package com.example.api_warehouse.Services;

import com.example.api_warehouse.Models.view_allRecord_tableinfor_web;
import com.example.api_warehouse.Repositorys.ViewAllRecord_Repo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ViewAllRecord_Service {
    @Autowired
    private ViewAllRecord_Repo viewAllRecord_repo;


    public List<view_allRecord_tableinfor_web> getViewAllRecord() {
        return viewAllRecord_repo.findAll();
    }
}
