package com.example.api_warehouse.Models;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;

@Data
@Document(collection = "view_allRecord_tableinfor_web")
public class view_allRecord_tableinfor_web {

    @Field("schema")
    private String schema;
    @Field("table_name")
    private String name;
    @Field("job_id")
    private String job_id;
    @Field("processDate")
    private Date processDate;
    @Field("countRecord")
    private int countRecord;
    @Field("countField")
    private int countField;
    @Field("month_id")
    private long month_id;
    @Field("date_string")
    private String date_string;
    @Field("version_No")
    private int version;

}
