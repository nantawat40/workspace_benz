package com.example.api_warehouse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiWarehouseApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiWarehouseApplication.class, args);
    }

}
