package com.example.api_warehouse.Controllers;

import com.example.api_warehouse.Models.view_allRecord_tableinfor_web;
import com.example.api_warehouse.Services.ViewAllRecordService_Interface;
import com.example.api_warehouse.Services.ViewAllRecord_Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/allRecord")
@Slf4j
public class ViewAllRecord {

    @Autowired
    ViewAllRecord_Service viewAllRecord_service;


    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<view_allRecord_tableinfor_web> getAllRecord() {
        log.info("Getting all record.");
        return viewAllRecord_service.getViewAllRecord();
    }

}
