package com.example.api_warehouse.Repositorys;

import com.example.api_warehouse.Models.view_allRecord_tableinfor_web;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ViewAllRecord_Repo extends MongoRepository<view_allRecord_tableinfor_web, Integer> {
}
