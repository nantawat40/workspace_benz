package com.example.api_warehouse.Services;

import com.example.api_warehouse.Models.view_allRecord_tableinfor_web;

import java.util.List;

public interface ViewAllRecordService_Interface {
    List<view_allRecord_tableinfor_web> getViewAllRecord();
}
