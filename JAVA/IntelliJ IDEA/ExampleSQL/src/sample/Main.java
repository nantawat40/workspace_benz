package sample;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    protected static Connection conn = null;

    public static void main(String[] args) {

        String url = "jdbc:mysql://localhost/hr";
        String user = "root";
        String password = "123456";

        getConnection(url, user, password);
        getEmployee(conn);
        dropAndCreateTable(conn);
        insertSelectFrom(conn);
        closeConnection();

        System.out.println("Run Finish");

    }


    private static void getConnection(String url, String user, String pass) {
        try {
            conn = DriverManager.getConnection(url, user, pass);
            System.out.println("Create Connection Seccess!");
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(0);
        }
    }

    private static void insertSelectFrom(Connection conn) {
        Statement stmt = null;
        String sql = "insert into NEW_DEPARTMENT (DEPARTMENT_ID, DEPARTMENT_NAME, MANAGER_ID, LOCATION_ID) " +
                "SELECT DEPARTMENT_ID, DEPARTMENT_NAME, MANAGER_ID, LOCATION_ID FROM departments;";
        try {
            stmt = conn.createStatement();
            int n = stmt.executeUpdate(sql);
            if (n != 0)
                System.out.println("Insert Data Success");
            else
                System.out.println("Insert Data Failed");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void dropAndCreateTable(Connection conn) {
        Statement stmt = null;
        String dropTable = "DROP TABLE IF EXISTS `NEW_DEPARTMENT`;";
        try {
            stmt = conn.createStatement();
            stmt.execute(dropTable);
            System.out.println("Drop Table IF Exists Success");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String sqls = "create table NEW_DEPARTMENT (DEPARTMENT_ID int primary key, " +
                      "DEPARTMENT_NAME varchar(40), MANAGER_ID int , LOCATION_ID int );";
        try {
            stmt.execute(sqls);
            System.out.println("Create Table 'NEW_DEPARTMENT' Success");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void getEmployee(Connection conn) {
        String sql = "SELECT DISTINCT salary FROM employees e1 order by salary desc";
        Statement stmt = null;
        ResultSet rs;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                if (rs.getRow() == 5) {
                    System.out.println("Salary = " + rs.getDouble("salary"));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void closeConnection() {
        if (conn != null) {
            try {
                conn.close();
                System.out.println("Connection Closed");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}

