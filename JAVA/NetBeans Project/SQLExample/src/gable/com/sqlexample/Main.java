/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gable.com.sqlexample;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
           
         Statement stmt = null;
        Connection conn = null ;
        String url = "jdbc:mysql://localhost/hr";
        String user = "root";
        String password = "123456";

        try { 
            conn = DriverManager.getConnection(url, user, password);
            System.out.println("Create Connection Seccess!");
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if (conn!=null){
            String sql = "select * from hr.employees";
            ResultSet rs = null;
            try { 
                //  Statement stmt = conn.prepareStatement("select * from hr.employee where employeeID=?");
               stmt = conn.createStatement();
              rs = stmt.executeQuery(sql);
              
              while(rs.next()){
                  String empID = rs.getString("EMPLOYEE_ID");
                  String empFName = rs.getString("FIRST_NAME");
                  String empLName = rs.getString("LAST_NAME");
                  String empEmail = rs.getString("EMAIL");
                  String empPhone = rs.getString("PHONE_NUMBER");
                  String empHire = rs.getString("HIRE_DATE"); 
                  String empSalary = rs.getString("SALARY");
                  String empCom = rs.getString("COMMISSION_PCT"); 
                  
                  System.out.println("EMPLOYEE_ID : "+ empID + " FIRST_NAME : " + empFName +
                          " LAST_NAME : " + empLName + " EMAIL : " + empEmail + " PHONE_NUMBER : " + empPhone + 
                          " HIRE_DATE : " +empHire+ " SALARY : " +empSalary+ " COMMISSION_PCT : " + empCom);
              } 
            } catch (SQLException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
                if(conn!=null) {
                    try {
                        //                  stmt.close();
                        conn.close();
                        stmt.close();
                        System.out.println("Connection Closed!");
                    } catch (SQLException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                  
                }
            }
        }
        System.out.println("Run Finish");

    }
    
}
