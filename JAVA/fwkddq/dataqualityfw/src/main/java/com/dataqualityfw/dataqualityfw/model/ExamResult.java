package com.dataqualityfw.dataqualityfw.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.*;

@Data
@Builder
@XmlRootElement(name = "ExamResult")
public class ExamResult {

	@XmlElement(name = "studentName")
    private String studentName;

	@XmlElement(name = "dob")
	private Date dob;

	@XmlElement(name = "percentage")
    private double percentage;    
}