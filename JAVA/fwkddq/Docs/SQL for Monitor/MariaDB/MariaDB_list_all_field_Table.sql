--1
select
	 a.`COLUMN_NAME`
	,a.`COLUMN_TYPE`
	,a.`IS_NULLABLE`
	,a.`COLUMN_KEY`
	,a.`COLUMN_DEFAULT`
	,a.`EXTRA`
from
	information_schema.`COLUMNS` a
where 
	    a.TABLE_SCHEMA = 'batch'
    and a.TABLE_NAME   = 'batch_job_execution'
order by a.ORDINAL_POSITION;


--2
desc batch.batch_job_execution;