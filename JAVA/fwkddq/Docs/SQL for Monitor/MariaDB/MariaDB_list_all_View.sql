select
	 a.TABLE_SCHEMA
	,a.TABLE_NAME
	,a.ROW_FORMAT
	,a.TABLE_ROWS
	,a.CREATE_TIME
	,a.UPDATE_TIME
	,a.CHECK_TIME	
from
	information_schema.tables a
where 
	 a.TABLE_TYPE = 'VIEW'
and (a.TABLE_SCHEMA <> 'mysql' and a.TABLE_SCHEMA <> 'performance_schema')
order by a.TABLE_SCHEMA, a.TABLE_NAME