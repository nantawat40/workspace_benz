--1
select        
	      a.COLUMN_NAME
	     ,a.DATA_TYPE
	     ,a.IS_NULLABLE
	     ,a.COLUMN_DEFAULT
	     ,a.character_maximum_length
	     ,a.numeric_precision
	     ,a.numeric_precision_radix
	     ,a.numeric_scale
from
	information_schema.COLUMNS a
where 
	    a.TABLE_SCHEMA = 'public'
    and a.TABLE_NAME   = 'account'
order by a.ORDINAL_POSITION;

