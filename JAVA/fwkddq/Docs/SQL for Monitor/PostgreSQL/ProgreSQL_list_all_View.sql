select
	   a.table_schema
	  ,a.TABLE_NAME
	--,a.table_type
	--,a.TABLE_ROWS
	--,a.CREATE_TIME
	--,a.UPDATE_TIME
	--,a.CHECK_TIME
from
	information_schema.views a
where 
	 --a.TABLE_TYPE = 'BASE TABLE'
	 1 = 1 
and (a.TABLE_SCHEMA <> 'information_schema' and a.TABLE_SCHEMA <> 'pg_catalog')
order by a.TABLE_SCHEMA, a.TABLE_NAME

