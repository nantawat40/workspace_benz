select
	 a.COLUMN_NAME
	,a.DATA_TYPE
	,a.IS_NULLABLE
	,case when a.CHARACTER_MAXIMUM_LENGTH is null then a.NUMERIC_PRECISION else a.CHARACTER_MAXIMUM_LENGTH end as Size
	,a.COLUMN_DEFAULT
from
	information_schema.COLUMNS a
where 
	    a.TABLE_SCHEMA = 'dbo'
    and a.TABLE_NAME   = 'MTRASSESS'
order by a.ORDINAL_POSITION;