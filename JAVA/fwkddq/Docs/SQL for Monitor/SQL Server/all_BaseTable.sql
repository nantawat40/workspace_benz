select
	 a.TABLE_SCHEMA
	,a.TABLE_NAME
from
	information_schema.tables a
where 
	 a.TABLE_TYPE = 'BASE TABLE'
order by a.TABLE_SCHEMA, a.TABLE_NAME