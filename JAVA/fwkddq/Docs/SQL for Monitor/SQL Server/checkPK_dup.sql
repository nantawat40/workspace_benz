select
	case 
	     when sum(Flag)  = 0 then 'False'
			 when sum(Flag) is null then 'False' 
			 else 'True' 
	End as IS_Duplicate
from
(
	select 
		BRANCHID, FORMULAID, 
		case count(*) when 1 then 0 else 1 end Flag
	from dbo.ACCOUNT_MAP_SAP
	group by BRANCHID, FORMULAID
	HAVING count(*) > 1
) t