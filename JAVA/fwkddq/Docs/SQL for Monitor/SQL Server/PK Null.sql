select 
	case 
		when sum(difference) != sum(countAll) then 'True' else 'False' end as result 
		from (
					select 
						count(*) as countAll,
						count(*) - 
							(select count(*) as H1 from batch.CSV_MULTISHEET where H1 = '' or H1 is null) as difference
					from batch.CSV_MULTISHEET
)t

-- SELECT   
-- 	case    
-- 	 when field_PK IS NULL then 'True' 
-- 	 when field_PK = '' then 'True' 
-- 	 else 'False' 
-- 	End as FK_IsNull
-- 	,
-- 	case 
-- 	 when field_PK is null then '(Null)' 
-- 	 when field_PK = '' then '(Empty)' 
-- 	 else 'False' 
-- 	End as IsNull_Detail 
-- 	FROM 
-- (
-- select WF_ID as field_PK
-- 	from dbo.ACTOR_INCIDENT as tbl
-- 	WHERE WF_ID is null or WF_ID = ''
-- 	GROUP BY tbl.WF_ID 
-- )b
-- 
-- 
