select
case 
		 when count(*) >= 1 then 'True'
		 when count(*) is null then 'False' 
		 else 'False' 
	End as FK_IsNull
	FROM 
(
	select 
	tbl_main.COMPANYID as key_main
	from dbo.ACTOR_INCIDENT as tbl_main
	where not exists(
			select 1 from dbo.ACTOR_PARAMETER where COMPANYID = tbl_main.COMPANYID
	)
) b
