select
	 a.COLUMN_NAME as P1 
	,a.DATA_TYPE as P2
	,a.IS_NULLABLE as P3
	,case when a.CHARACTER_MAXIMUM_LENGTH is null then a.NUMERIC_PRECISION else a.CHARACTER_MAXIMUM_LENGTH end as P4
	,a.COLUMN_DEFAULT
from
	information_schema.COLUMNS a
where 
	    a.TABLE_SCHEMA = 'dbo'
    and a.TABLE_NAME   = 'MTRASSESS'
order by a.ORDINAL_POSITION;