package com.example.mongodb.demomongo.mongoDB;

import java.util.List;

import com.example.mongodb.demomongo.model.ExamResult;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface ExamResultRepository extends MongoRepository<ExamResult, String> {

    public ExamResult findByStudentName(String studentName);

    @Query("{'percentage': {$lt: ?0}}")
    public List<ExamResult> findByGrade(Double i);
}