package com.example.mongodb.demomongo;

import java.util.List;

import com.example.mongodb.demomongo.model.ExamResult;
import com.example.mongodb.demomongo.mongoDB.ExamResultRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

	@Autowired
	private ExamResultRepository repository;
	
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		repository.deleteAll();

		//create test data
		ExamResult student1 = new ExamResult("Dit P", "10/07/1990", 90);
		ExamResult student2 = new ExamResult("Dit E", "11/07/1990", 80);
		ExamResult student3 = new ExamResult("Dit R", "12/07/1990", 70);
		repository.save(student1);
		repository.save(student2);
		repository.save(student3);

		//find a car by model
		ExamResult ditP = repository.findByStudentName("Dit P");
		log.info("Student: {}", ditP);

		//find cars with horse power less than 200
		List<ExamResult> students = repository.findByGrade(75.0);

		//log the results
		log.info("Found the following Student");
		for (ExamResult foundStudent: students){
			log.info(foundStudent.toString());
		}
	}

}
