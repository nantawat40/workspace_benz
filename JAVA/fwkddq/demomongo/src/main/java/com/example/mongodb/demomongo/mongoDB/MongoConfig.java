package com.example.mongodb.demomongo.mongoDB;

import java.util.List;

import com.example.mongodb.demomongo.model.ExamResult;
import com.mongodb.MongoClient;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import lombok.extern.slf4j.Slf4j;

@Slf4j
// @Configuration
@EnableMongoRepositories(basePackages = "com.gable.dos.ddq.mongoDB")
public class MongoConfig extends AbstractMongoConfiguration {

	private ExamResultRepository examResultrRepository;
  
    @Override
    protected String getDatabaseName() {
        return "batch";
    }
  
    @Override
    public MongoClient mongoClient() {
        return new MongoClient("127.0.0.1", 27017);
    }
  
    @Override
    public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongoClient(), getDatabaseName());
    }

    @Override
    protected String getMappingBasePackage() {
        return "com.gable.dos.ddq.mongoDB";
    }

    @Bean
	public void insertMongoDB() throws Exception {
        //Clean the collection for our test
		// examResultrRepository.deleteAll();

		//create test data
		ExamResult student1 = new ExamResult("Dit P", "10/07/1990", 90);
		ExamResult student2 = new ExamResult("Dit E", "11/07/1990", 80);
		ExamResult student3 = new ExamResult("Dit R", "12/07/1990", 70);
		examResultrRepository.save(student1);
		examResultrRepository.save(student2);
		examResultrRepository.save(student3);

		//find a car by model
		ExamResult ditP = examResultrRepository.findByStudentName("Dit P");
		log.info("Student: {}", ditP);

		//find cars with horse power less than 200
		List<ExamResult> students = examResultrRepository.findByGrade(75.0);

		//log the results
		log.info("Found the following Student");
		for (ExamResult foundStudent: students){
			log.info(foundStudent.toString());
		}

	}
    
}