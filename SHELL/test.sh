#!/bin/bash

#--------------------------------------------------------------------
#              Copyright C - 2012 All rights reserved
#                        G-Able Thailand.
#
#       No part of this program may be reproduced or adapted in any
#             form or by any means, electronic or mechanical,
#       without permission from G-Able Thailand Thailand.
#          This program is confidential and may not be disclosed,
#          decompiled or reverse engineered without permission in
#             writing from G-Able Thailand Thailand.
#
#--------------------------------------------------------------------
# DESCRIPTION:
#    create table/view/object on database on area staging
#
#--------------------------------------------------------------------
# Revision   Date        CR No.   Author         Description
#====================================================================
#
#
#
#

echo "File Name: $0"
echo "First Parameter : $1"
echo "Second Parameter : $2"
echo "Quoted Values: $@"
echo "Quoted Values: $*"
echo "Total Number of Parameters : $#"

for TOKEN in $@; do
    echo $TOKEN
done

##
## Array ##
##

NAME[0]="NANTAWAT"
NAME[1]="ADISAK"
NAME[2]="Mahnaz"
NAME[3]="Ayan"
NAME[4]="Daisy"

numA=$(expr 3 % 4)
echo "numA=$numA"
numB= $numA
echo "numB=$numB"
echo [ $numA == $numB ]

echo ${NAME[*]}

read a
