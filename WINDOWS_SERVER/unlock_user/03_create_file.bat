@echo off  
setlocal ENABLEDELAYEDEXPANSION
set PWD=%cd%
set PS_PATH=%PWD%\INPUT\
echo %PS_PATH%

REM SET Current DateTime
for /f "tokens=1-4 delims=/ " %%i in ("%date%") do ( 
     set dow=%%i
     set month=%%j
     set day=%%k
     set year=%%l
)
set datestr=%year%%month%%day%
echo date is: %datestr%


set hour=%time:~0,2%
if "%hour:~0,1%" == " " set hour=0%hour:~1,1%
REM echo hour=%hour%
set min=%time:~3,2%
if "%min:~0,1%" == " " set min=0%min:~1,1%
REM echo min=%min%
set secs=%time:~6,2%
if "%secs:~0,1%" == " " set secs=0%secs:~1,1%
REM echo secs=%secs%
set timess=%hour%%min%%secs%
echo time is: %timess%


for /l %%i in (1 1 1) do (
	for /l %%u in (1 1 9) do (
	echo ""> %PS_PATH%user0%%u_%datestr%%timess%.AD_Unlock
	)
)
echo Create order file in "%PS_PATH%<AD Username>_<DateTime>.AD_Unlock"