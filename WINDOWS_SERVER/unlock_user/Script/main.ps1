﻿$PATH_LIB = "C:\Program Files\System.Data.SQLite\2012\bin\System.Data.SQLite.dll"
# initailize date/time
$current_date = Get-Date -Format "yyyy-MM-dd"
$current_time = Get-Date -Format "HH:MM:ss" 
# initailize path 
$LOCATION = Get-Location 
#$LOCATION = "C:\Users\user_unlock01\Desktop\unlock_userCOpy"
# $INPUT_PATH = "Z:\shared_dir2012R2(3)_WIN10PRO\order\AD\"
$INPUT_PATH = "$LOCATION\INPUT"
$OUTPUT_PATH = "$LOCATION\OUTPUT"
$ARCHIVE_PATH = "$OUTPUT_PATH\ARCHIVE\$current_date\"
# $WARNING_PATH = "$OUTPUT_PATH\WARNING\$current_date\"
# $ERROR_PATH = "$OUTPUT_PATH\ERROR\$current_date\"
$LOG_PATH = "$OUTPUT_PATH\LOGS\$current_date"

Add-Type -Path $PATH_LIB
# new Object for SQLite
$con = New-Object -TypeName System.Data.SQLite.SQLiteConnection

# initailize path database file (SQLite)
$db_file = "C:\sqlite\unlock_userDB.db"

# initailize number locked daily limit  
$max_locked = 5
$num_locked = 0

$Logfile = "$LOG_PATH\$(Get-Content env:computername).log"  
#Function that logs a message to a text file


if (!(Test-Path -Path $INPUT_PATH )) {
    LogWarning "$INPUT_PATH doesn't exist input path locations."
    exit(0)
} 
if (!(Test-Path -Path $ARCHIVE_PATH )) {
    New-Item -ItemType directory -Path $ARCHIVE_PATH
}
# if(!(Test-Path -Path $WARNING_PATH )){
#     New-Item -ItemType directory -Path $WARNING_PATH}
# if(!(Test-Path -Path $ERROR_PATH )){
#     New-Item -ItemType directory -Path $ERROR_PATH}
if (!(Test-Path -Path $LOG_PATH )) {
    New-Item -ItemType directory -Path $LOG_PATH
}



function LogWrite {   
    param([string]$Message)
    Write-Host $Message
    ((Get-Date -Format "dd/MM/yyyy HH:mm:ss").ToString() + " [info] - " + $Message) >> $LogFile;
} 
function LogWarning {   
    param([string]$Message)
    Write-Warning $Message
    ((Get-Date -Format "dd/MM/yyyy HH:mm:ss").ToString() + " [Warning] - " + $Message) >> $LogFile;
}


if (!(Test-Path -Path $PATH_LIB )) {
    LogWarning "$PATH_LIB doesn't exist in both locations."
    exit(0)
}  

function connectDB() {
    if (!([System.IO.File]::Exists($db_file))) {
        LogWarning "$db_file doesn't exist in both locations."
        exit(0);
    }
    $con.ConnectionString = "Data Source=$db_file"
    $con.Open() 
    LogWrite "connectDB Successfull."
}


function Err_Warn($code_) {  
    LogWarning $code_[1]
    switch ($code_[0]) {
        "e" {
            backup_Data ("e", $ARCHIVE_PATH) 
            chk_backup_Data $ARCHIVE_PATH; break
        }
        "w" {
            backup_Data ("w", $ARCHIVE_PATH) 
            chk_backup_Data $ARCHIVE_PATH; break
        } 
        "b" { ""; break }
        Default { "Err_Warn function in case default"; break }
    }   
    break
}

function Check_ADUser() {
    Try {
        Get-ADUser $user_name -Properties LockedOut -ErrorAction Stop 
    }
    Catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException] { 
        Err_Warn "e", "Cannot find this Username : $user_name"  ; break
    } 
}

function CheckCondition($number_locked) { 
    #Check CountNumberLocked
    if ($number_locked -ge $max_locked) { 
        Write-Host "TESSSS"
        Err_Warn("w", "this Account $user_name locked (daily limit exceeded)")
    } 
    #Check DateTime  
    # [int]$date_ = ($Order_Date).Substring(6) #30  
    # [int]$month_ = (($Order_Date).Substring(4)).Substring(0, 2) #04
    # [int]$year_ = ($Order_Date).Substring(0, 4) #2020

    # [int]$hour_ = ($Order_Time).Substring(0, 2) #16
    # [int]$minute_ = (($Order_Time).Substring(2)).Substring(0, 2) #16

    # $DateTimeOrder = Get-Date  -Month $month_ -Day $date_ -Year $year_ -Hour $hour_ -Minute $minute_
    $Start_not_unlock = Get-Date -Hour 00 -Minute 1 
    $End_not_unlock = Get-Date -Hour 05 -Minute 59

    # if($DateTimeOrder.TimeOfDay -gt $Start_not_unlock.TimeOfDay -and $DateTimeOrder.TimeOfDay -lt $End_not_unlock.TimeOfDay  ){
    #     Err_Warn("w","Sorry, the system will not work during $Start_not_unlock - $End_not_unlock")
    # } 

    $DateTimeNow = Get-Date
    if ($DateTimeNow.TimeOfDay -gt $Start_not_unlock.TimeOfDay -and $DateTimeNow.TimeOfDay -lt $End_not_unlock.TimeOfDay  ) {
        Err_Warn("w", "Sorry, the system will not work during $Start_not_unlock - $End_not_unlock")
    }
} 


function Initial_data() {
    LogWrite "Initialize Data" 
    $sql.CommandText = "INSERT INTO user_order (USER_NAME,COUNT_LOCKED,PROCESS_DATE,PROCESS_TIME) VALUES ('$user_name',0,'$current_date','$current_time')"
    $adapter = New-Object -TypeName System.Data.SQLite.SQLiteDataAdapter $sql

    $data = New-Object System.Data.DataSet
    [void]$adapter.Fill($data)  
}

function addNumberOfLocked() {
    $num = $num_locked + 1
    $sql_add = $con.CreateCommand() 

    $sql_add.CommandText = "UPDATE user_order SET COUNT_LOCKED = $num WHERE USER_NAME ='$user_name' and PROCESS_DATE = '$current_date'"
 
    $adapter_add = New-Object -TypeName System.Data.SQLite.SQLiteDataAdapter $sql_add

    $data_add = New-Object System.Data.DataSet
    [void]$adapter_add.Fill($data_add) 
    # $data_set = $data_add.tables.rows 
}


function check_unlockUser($code) { 
    $confirm_data = Get-ADUser $user_name -Properties LockedOut | Where-Object { $_.LockedOut -eq $False } | Out-String 
    
    if (!$confirm_data) {
        LogWrite "Check Unlock User :: FALSE" 
        Err_Warn("w", "Check Unlock UserAD is : Failed")
    }
    else {
        
        if ($code -ne 1) {
            LogWrite "Check Unlock User :: TRUE"
            LogWrite  $confirm_data
            addNumberOfLocked
        }
        else {
            Err_Warn("w", " This Username: $user_name is not locked.")
        }
    }
}

function backup_Data($PATHS) {
    $PATH = $PATHS[1]
    $STATUS = ""
    switch ($PATHS[0]) {
        "e" { $STATUS = "error"; Break }
        "w" { $STATUS = "warning"; Break } 
        "p" { $STATUS = "success"; Break }
        Default { $STATUS = "error"; Break }
    }  

   
    $new_filename = ($file_name + "_$STATUS." + $file_extension)
    
    # echo  $new_filename
    Move-Item -Path $INPUT_PATH\$file_input -Destination $PATH$new_filename -Force
    LogWrite "Backup Data from Order path to $PATH$new_filename"
}

function chk_backup_Data($PATH) { 
    $Chk_INPUTFILE = Test-Path $INPUT_PATH\$file_input #False
    $Chk_OUTPUTFILE = Test-Path $PATH$new_filename #True
    
    if (!$Chk_INPUTFILE -and $Chk_OUTPUTFILE) {
        LogWrite "Check Backup Data :: TRUE"
    }
    else {
        LogWrite "Check Backup Data :: FALSE"
        Err_Warn("b", "File Backup An error has occurred.")
    }
}

function get_data() {
    $sql = $con.CreateCommand()
    
    #State Select
    $sql.CommandText = "SELECT * FROM user_order WHERE USER_NAME ='$user_name' and PROCESS_DATE = '$current_date' ORDER BY rowid DESC"
 
    $adapter = New-Object -TypeName System.Data.SQLite.SQLiteDataAdapter $sql

    $data = New-Object System.Data.DataSet
    [void]$adapter.Fill($data) 
    $data_set = $data.tables.rows 


    if (!$data_set) {
        LogWrite "this user $user_name no data today" 
        Initial_data
        LogWrite "geting data again"
        get_data
    }
    else {
        $num_locked = $data_set['COUNT_LOCKED'];
        LogWrite "user_name = $user_name"
        LogWrite "last num_locked = $num_locked"
        Checkcondition($num_locked) 
        LogWrite "In progress Unlock user account $user_name"
        #Execute Unlock User
        $code , $massages = &"$LOCATION\Script\unlock_user.ps1" -u $user_name -y  | Select-Object -Last 2
        #LogWrite  "Return Code :"$code 
        
        switch ($code) {
            0 {
                check_unlockUser $code 
                backup_Data ("p", $ARCHIVE_PATH) 
                chk_backup_Data $ARCHIVE_PATH;
                
                Break 
            }  #Pass
            1 { check_unlockUser $code; Break }  #User is not locked
            101 { Err_Warn "e", $massages ; Break }        #Cannot find this Username
            Default { Err_Warn("e", "Return Code Not match"); Break }
        }
        LogWrite "Status : $massages"   
    }
}  

function Polling {
    $arr = Get-ChildItem -Path $INPUT_PATH\* -Include *.AD_Unlock | Select-Object -expand Name
 
    if ($arr.Length -gt 0) {
        foreach ($file_input in $arr) {
            LogWrite "`r`n"
            LogWrite "Starting Process."
            LogWrite "ORDER NAME :: $file_input"
            $CharArray = ($file_input).Split("_") #user01, 20200430161622.AD,Unlock
            $CharArray2 = ($file_input).Split(".") #user01_20200430161622,AD_Unlock
            
            $user_name = $CharArray[0] #user01
            $file_name = $CharArray2[0] #user01_20200430161622
            $file_extension = $CharArray2[1] # AD_Unlock
            $Order_datetime = ($CharArray[1]).Split(".") # 20200430161622, AD
            $Order_Date = ($Order_datetime[0]).Substring(0, 8) #20200430
            $Order_Time = ($Order_datetime[0]).Substring(8) # 161622
            $new_filename = ""
            
            get_data
            LogWrite "End Process."
        }
    }
    else {
        Write-Host "========"
        Write-Host "NO ORDER"
        Write-Host "========"
        $delay = 10 
        while ($delay -gt 0) { 
            Write-Host "Seconds Remaining: $($delay)" 
            start-sleep 1
            $delay -= 1
        } 
    }
    Polling
}
connectDB
Polling
#CMD /c PAUSE

