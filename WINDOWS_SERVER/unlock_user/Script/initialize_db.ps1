$PATH_LIB="C:\Program Files\System.Data.SQLite\2012\bin\System.Data.SQLite.dll"
$LOCATION_DB_PATH="C:\sqlite"
if(!(Test-Path -Path $PATH_LIB )){
    Write-Warning "$PATH_LIB doesn't exist in both locations."
    exit(0)
}  
if(!(Test-Path -Path $LOCATION_DB_PATH )){
    New-Item -ItemType directory -Path $LOCATION_DB_PATH}


function connectDB() { 
    Add-Type -Path "C:\Program Files\System.Data.SQLite\2012\bin\System.Data.SQLite.dll"

    $db_file = "$LOCATION_DB_PATH\unlock_userDB.db" 
    if ([System.IO.File]::Exists($db_file)){ Remove-Item  $db_file}
    sqlite3 $db_file .databases 
    # new Object for SQLite
    $con = New-Object -TypeName System.Data.SQLite.SQLiteConnection  
    $con.ConnectionString = "Data Source=$db_file"
    $con.Open() 
    $sql = $con.CreateCommand() 
    $sql.CommandText = "CREATE TABLE user_order (USER_NAME VarChar(20), COUNT_LOCKED int(5), PROCESS_DATE VarChar(20), PROCESS_TIME VarChar(20));"  
    $sql.ExecuteNonQuery()  

    $sql.Dispose()   
}

connectDB  
