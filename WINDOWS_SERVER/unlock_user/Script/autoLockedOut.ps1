﻿#Requires -Version 3.0
#Requires -Modules ActiveDirectory, GroupPolicy
if ((([xml](Get-GPOReport -Name "Default Domain Policy" -ReportType Xml)).GPO.Computer.ExtensionData.Extension.Account |
            Where-Object name -eq LockoutBadCount).SettingNumber) {
    #num = 0
    $Password = ConvertTo-SecureString 'NotMyPassword' -AsPlainText -Force
    Get-ADUser -Filter 'Name -like "user*"' -Properties SamAccountName, UserPrincipalName, LockedOut |
        ForEach-Object {
            Do {
      #          Write-Output $num
                Invoke-Command -ComputerName dc01 {Get-Process
                } -Credential (New-Object System.Management.Automation.PSCredential ($($_.UserPrincipalName), $Password)) -ErrorAction SilentlyContinue
                
            }
            Until ((Get-ADUser -Identity $_.SamAccountName -Properties LockedOut).LockedOut)
            Write-Output "$($_.SamAccountName) has been locked out"
        }
}