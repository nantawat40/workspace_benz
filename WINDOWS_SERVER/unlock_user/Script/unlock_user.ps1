﻿# Initial variable
$user_name = ''
$chk_confirm = "no"

# Chack args
$num_args = $args.Length

function exits($code ,$mass){ 
    # if ($code -ne 0){
    #     Write-Warning "$code $mass"
    # }
    Write-Output $code $mass
    exit($code)
}

if ($num_args -lt 2 -or $num_args -gt 3) {
    $massage = "Invalid parameters: unlock_user.ps1 -u <user_name> [-y]"
    exits 1 $massage
}


# Initial args
for ($i = 0; $i -lt $num_args; $i++) {
    if ($args[$i] -eq '-u' -or $args[$i] -eq '-U') {
    
        $user_name = $args[$i + 1] 
    }
    if ($args[$i] -eq '-y' -or $args[$i] -eq '-Y') {
    
        $chk_confirm = "yes"
    }
} 
if ($user_name -eq '' ) {
     $massage = "Unlock Failed! Invalid parameters: unlock_user.ps1 -u <user_name> [-y]"
    exits 1 $massage
}
 


# Test Get-ADUser
Try {
    Get-ADUser $user_name -Properties LockedOut -ErrorAction Stop 
}
Catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException] { 
    
    $massage = "Unlock Failed! Cannot find this Username : $user_name" 
    exits 101 $massage
}  

# Test Get-ADUser 
$chk_IsFalse = Get-ADUser $user_name -Properties LockedOut | Where-Object { $_.LockedOut -eq $True }
 

if (!$chk_IsFalse) {
     $massage = "Unlock Failed! This Username: $user_name is not locked." 
     exits 1 $massage  
} 

# function check_confirm_unlock 
function check_confirm_unlock {
    if ($chk_confirm -eq 'yes') { 
    
        unlock_user
         $massage = 'Unlock Success!'
        exits 0 $massage
    }
    else {
        $confirm = Read-Host "Are you sure you want to perform this action? (y / n)"
    }

 
    if ($confirm -eq 'y' -or $confirm -eq 'Y') {
        unlock_user
    }
    elseif ($confirm -eq 'n' -or $confirm -eq 'N') {
        Write-Output "Cancel to action Thank you."
    }
    else {
        check_confirm_unlock
    }
}

# function unlock_user 
function unlock_user { 
    Write-Output "Unlocking..."
    Get-ADUser $user_name -Properties LockedOut | Where-Object { $_.LockedOut -eq $True } | Unlock-AdAccount   
    #Get-ADUser $user_name -Properties LockedOut 
    Write-Output "This user account $user_name has been successfully unlocked."
    Write-Output "Thank you."
}

check_confirm_unlock




